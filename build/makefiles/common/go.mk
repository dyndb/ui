
# PROJECT_NAME := "dyndb"
# PKG := "gitlab.com/nerdhaltig/dyndb/backend"
# PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/ | grep -v boltdb  )

# update-modules: ## Update all go modules
# 	go get -u ./...
# 	go get -t -u ./...
# 	go mod tidy
# 	go mod vendor

# test: ./coverage.out ## run tets

# ./coverage.out:
# 	go test -race -coverprofile $@ -v $(PKG_LIST)

# ./.testCoverage.txt: ./coverage.out
# 	mv -v $< $@

# ./report.xml:
# 	gotestsum --junitfile report.xml --format testname
