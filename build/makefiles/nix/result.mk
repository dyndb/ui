.ONESHELL:

build/output/nix/%:
	$(call in_nix_shell)
	
	$(eval OUT_LINK := $(shell echo "$@"))
	$(eval OUT_TARGET := $(shell echo "$@" | sed 's/build\/output\/nix\///g'))
	echo -e "$(DEBUG) Build $(OUT_TARGET) to $(OUT_LINK) $(NORMAL)"
	nix-build -A $(OUT_TARGET) --out-link $(OUT_LINK) build/nix/default.nix

clean: clean-result
clean-result:
	@rm -fRv build/output/nix

