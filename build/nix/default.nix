{ pkgs ? import (import ./nixpkgs.nix) { }
, packageVersion ? (import ../../version.nix).version
}:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;

  ################################### Binary ###################################
  src = pkgs.nix-gitignore.gitignoreSourcePure [
    ".gitlab-ci.yml"
    ".gitignore"
    "build/"
    "dist/"
    "node_modules/"
    "result/"
    "flake*"
    "*.adoc"
    "*.xml"
    "*.log"
  ]
    (pkgs.lib.cleanSource ./../..);


  # the binary
  package = import ./package.nix {
    inherit pkgs;
    inherit src;
    inherit packageVersion;
  };



in
{
  package = package;
}
