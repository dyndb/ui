# nix-build -E 'with import <nixpkgs> { }; callPackage ./default.nix { packageVersion = "latest"; }'
{ pkgs, src, packageVersion ? "develop" }:
let
  lib = pkgs.lib;
  callPackage = lib.callPackageWith (pkgs // pkgs.lib);
  inherit (lib) sourceByRegex;
  inherit (pkgs) mkYarnPackage;

in
mkYarnPackage {
  name = "dyndb-ui-${packageVersion}";
  src = sourceByRegex ./../.. [
    "^package.json"
    "^yarn.lock"
    "src"
    "src/.*"
    "public"
    "public/.*"
    "index.html"
    "^tsconfig.json"
    "^tsconfig.node.json"
    "^tsconfig.dyndb.json"
    "^tsconfig.app.json"
    "^vite.config.ts"
  ];
  version = packageVersion;

  yarnNix = ./yarn.nix; # this is needed if you cross-build stuff
  yarnLock = ./../../yarn.lock;
  packageJSON = ./../../package.json;

  patches = [ ];

  buildPhase = ''
    echo -e "export const version = \"${packageVersion}\"" > deps/ui/src/api/version.ts
    yarn --offline build
  '';

  distPhase = "true";

  installPhase = ''
    mkdir -p $out
    cp -vR deps/ui/dist/* $out/
  '';
}

