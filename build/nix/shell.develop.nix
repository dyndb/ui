# run it with nix-shell yarn.shell.nix

{ pkgs ? import <nixpkgs> { } }:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;

in
pkgs.mkShell {
  buildInputs = with pkgs;
    [
      yarn
      yarn2nix

      python3
      nodePackages.node-gyp-build
      fontconfig
      fontconfig.dev
    ];
  shellHook = ''
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${pkgs.pixman}/lib/pkgconfig
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${pkgs.cairo.dev}/lib/pkgconfig
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${pkgs.libpng.dev}/lib/pkgconfig
    
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${pkgs.glib.dev}/lib/pkgconfig
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${pkgs.harfbuzz.dev}/lib/pkgconfig
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${pkgs.freetype.dev}/lib/pkgconfig
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:${pkgs.pango.dev}/lib/pkgconfig

    pkg-config pixman-1 --libs
    pkg-config cairo --libs
    pkg-config libpng --libs
    pkg-config pangocairo --libs
    pkg-config freetype2 --libs
  '';
}
