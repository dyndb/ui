# run it with nix-shell yarn.shell.nix

{ pkgs ? import (import ./nixpkgs.nix) { } }:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;

in
pkgs.mkShell {
  buildInputs = with pkgs;
    [
      yarn
      yarn2nix
    ];
  shellHook = ''
  '';
}
