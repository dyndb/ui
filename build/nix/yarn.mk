.ONESHELL:

release-prepare: ./build/nix/yarn.nix ## prepare to build the package
package-prepare: ./build/nix/yarn.nix ## prepare to build the package
./build/nix/yarn.nix:
	$(call in_nix_shell)
	yarn2nix --lockfile=./yarn.lock > $@

clean: clean-yarn
clean-yarn:
	@rm -fv ./build/nix/yarn.nix
 