import { farFilePdf, fasFile, fasImage, fasCircleQuestion } from '@quasar/extras/fontawesome-v6'

export function RandomID(): string {
  return '_' + Math.random().toString(36).substr(2, 9)
}

export function HumanReadableBytes(n: number): string {
  const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  //let l = 0, n = parseInt(x, 10) || 0;

  let l = 0
  while (n >= 1024 && ++n) {
    n = n / 1024
    l++
  }
  return n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]
}

export function GetIconFromMimeType(mime: string) {
  if (mime.startsWith('text/plain')) {
    return fasFile
  }
  if (mime.startsWith('image/jpeg')) {
    return fasImage
  }
  if (mime.startsWith('application/pdf')) {
    return farFilePdf
  }

  console.warn(`unknown mime-type '${mime}', we use a default icon`)
  return fasCircleQuestion
}

export function trimAny(str: string, chars: string): string {
  let start = 0
  let end = str.length

  while (start < end && chars.indexOf(str[start]) >= 0) ++start

  while (end > start && chars.indexOf(str[end - 1]) >= 0) --end

  return start > 0 || end < str.length ? str.substring(start, end) : str
}
