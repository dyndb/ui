export function exchange<T>(arr: T[], from: number, to: number): void {
  if (from < 0 || from >= arr.length) {
    throw new Error('Invalid index')
  }

  if (to < 0 || to >= arr.length) {
    throw new Error('Invalid index')
  }

  const oldValue = arr[to]
  arr[to] = arr[from]
  arr[from] = oldValue
}

export function toLeft<T>(arr: T[], index: number): void {
  if (index === 0) {
    return // No need to shift if element is already at the leftmost position
  }

  exchange(arr, index, index - 1)
}
export function toRight<T>(arr: T[], index: number): void {
  if (index === 0) {
    return // No need to shift if element is already at the leftmost position
  }

  exchange(arr, index, index + 1)
}
