import { useDocumentsStore } from '@/stores/documents'
import api from '.'
import { type JSONSingleDoc } from './document'
import type { JSONPagedDoc, JSONPagedDocLinks } from './documents'
import { errors } from './error'
import type { dynDBSearchRequest } from './search'

export type AttachmentAction = "preview" | "delete" | "collect";


// ddbattachments
export interface DynDBAttachmentMetadata {
  displayText: string
  faicon: string | undefined
  filenameoriginal: string | undefined
  bytes: number | undefined | undefined
  mimetype: string | undefined
  uploaddatetime: Date | undefined

  // bucket
  blobbucket: string | undefined
  blobid: string | undefined

  docid: string[]

  _links: DynDBAttachmentLinks
}

export interface DynDBAttachmentLinks {
  self: string
  download?: string | undefined
  delete?: string | undefined
  preview?: string | undefined
}

export interface LinkToDocumentOpts {
  DocID: string
  MetadataID: string
  DisplayText: string
  Mimetype: string
}

export class DynDBAttachment {



  public async FetchRecent() {
    // store
    const documentsStore = useDocumentsStore()

    const documents: Record<string, JSONSingleDoc> = {}
    const links: JSONPagedDocLinks = {}

    const FetchFromURL = {
      url: "/attachment/list/short/0/25",
      payload: undefined,
      documents: documents,
      links: links
    }
    const err = await api.documents.FetchFromURL(FetchFromURL)
    if (err === null) {
      documentsStore.Set(FetchFromURL.documents)
      documentsStore.links = FetchFromURL.links
    } else {
      documentsStore.Set({})
      documentsStore.links = {}
    }
  }

  public async Fetch(url: string | undefined): Promise<DynDBAttachmentMetadata> {
    if (url === undefined) { throw errors.ParameterUndefined }

    const response = await api.request.get(url, '')
    if (response.status !== 200) { throw errors.LoadError(response.data) }

    return response.data as DynDBAttachmentMetadata
  }

  public async Search(words: string[]) {
    const documentsStore = useDocumentsStore()
    if (words.length === 0) return

    // if its empty
    if (words.length === 1) {
      if (words[0] === '') {
        documentsStore.$reset()
        return
      }
    }

    const FetchFromURLOptions = {
      url: '/attachment/search/short/0/25',
      payload: {
        words: words
      } as dynDBSearchRequest,
      documents: {},
      links: {}
    }

    const err = await api.documents.FetchFromURL(FetchFromURLOptions)

    if (err === null) {
      documentsStore.Append(FetchFromURLOptions.documents)
      documentsStore.links = FetchFromURLOptions.links
    }

  }

  public async Upload(url: string | undefined, files: readonly File[] | null): Promise<true | Error> {
    if (url === undefined) return errors.UploadNotPossible

    if (files === null) {
      return errors.NoDocument
    }

    const formData = new FormData()
    for (const fileIndex in files) {
      const file = files[fileIndex]
      formData.append('documents', file)
    }

    const response = await api.request.post(url, formData)

    console.debug(response)

    if (response.status === 200) {
      return true
    }
    return { name: 'upload error', message: response.data } as Error
  }

  public async Download(url: string | undefined) {
    if (url === undefined) return errors.UploadNotPossible
    await api.request.downloadFromURL(url)
  }

  public async Delete(url: string | undefined) {
    if (url === undefined) return errors.UploadNotPossible
    const response = await api.request.delete(url, '')
    if (response.status === 200) {
      return true
    }
  }

  public async FetchPreview(url: string | undefined) {
    if (url === undefined) return errors.UploadNotPossible
    await api.request.downloadFromURL(url)
  }

  public async LinkToDocument(url: string | undefined, opts: LinkToDocumentOpts) {
    if (url === undefined) return errors.ParameterUndefined

    api.request.post(url, opts)

    return
  }

  /**
   * Format bytes as human-readable text.
   *
   * @param bytes Number of bytes.
   * @param si True to use metric (SI) units, aka powers of 1000. False to use
   *           binary (IEC), aka powers of 1024.
   * @param dp Number of decimal places to display.
   *
   * @return Formatted string.
   */
  public HumanFileSize(bytes: number, si = false, dp = 1) {
    const thresh = si ? 1000 : 1024

    if (Math.abs(bytes) < thresh) {
      return bytes + ' B'
    }

    const units = si
      ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
      : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
    let u = -1
    const r = 10 ** dp

    do {
      bytes /= thresh
      ++u
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1)

    return bytes.toFixed(dp) + ' ' + units[u]
  }
}
