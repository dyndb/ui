import api from '.'
import { useLoginStore } from '../stores/login'

export interface SessionInfo {
  tenant: string
  username: string
}

export class DynDBAuth {
  /**
   * Fetch information about the logged-in user
   * this cause
   * @returns
   */
  public async FetchUserInfo() {
    const loginStore = useLoginStore()

    try {
      const response = await api.request.get('/me', '')
      response.data

      if (response.status === 200) {
        loginStore.$state.loggedin = true
      } else {
        loginStore.$state.loggedin = false
      }

      return response.data as SessionInfo
    } catch {
      loginStore.$state.loggedin = false
      return undefined
    }
  }

  public async LogIn(tenant: string, username: string, password: string) {
    const response = await api.request.post('/login', {
      tenant,
      username,
      password
    })

    const store = useLoginStore()

    if (response.status === 200) {
      store.$patch({
        loggedin: true,
        username: username
      })
    } else {
      store.$patch({
        loggedin: false,
        username: ''
      })
    }
  }

  public async Logout() {
    const store = useLoginStore()

    store.$patch({
      loggedin: false,
      username: ''
    })
  }
}
