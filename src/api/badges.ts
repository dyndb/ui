import type { Badge } from "@/components/dyndb/common/DataCards.vue";


export const BadgeDocument = {
    displayText: 'Document',
    color: 'blue-grey-5',
} as Badge

export const BadgeField = {
    displayText: 'Field',
    color: 'blue-grey-5',
} as Badge

export const BadgeTemplate = {
    displayText: 'Template',
    color: 'blue-grey-5',
} as Badge

export const BadgeAttachment = {
    displayText: 'Attachment',
    color: 'cyan-9',
} as Badge