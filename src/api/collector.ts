import { useCollectionStore } from '../stores/collection'
import { type JSONSingleDoc } from './document'

export type CollectorActionMode = "select" | "uncollect";


export class DynDBCollector {
  public constructor() { }

  public async CollectDocument(doc: JSONSingleDoc | undefined) {
    const collectionStore = useCollectionStore()
    collectionStore.CollectDocument(doc)
  }

  public async UnCollect(id: string | undefined) {
    const collectionStore = useCollectionStore()
    collectionStore.ForgetDocument(id)
  }

}
