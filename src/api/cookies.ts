// returns the cookie with the given name,
// or undefined if not found
export async function getCookie(name: string): Promise<string | undefined> {
  const cookies = document.cookie
  const value = `; ${cookies}`
  const parts = value.split(`; ${name}=`)

  if (parts.length === 2) {
    const result = parts.pop()?.split(';').shift()
    return result
  }
}

export async function setCookie(name: string, value: string, options = {} as any) {
  options = {
    path: '/',
    // add other defaults here if necessary
    ...options
  }

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString()
  }

  let updatedCookie = encodeURIComponent(name) + '=' + encodeURIComponent(value)

  for (const optionKey in options) {
    updatedCookie += '; ' + optionKey
    const optionValue = options[optionKey]
    if (optionValue !== true) {
      updatedCookie += '=' + optionValue
    }
  }

  document.cookie = updatedCookie
  return
}

// Example of use:
//setCookie('user', 'John', {secure: true, 'max-age': 3600});

export async function deleteCookie(name: string) {
  setCookie(name, '', {
    'max-age': -1
  })

  return
}
