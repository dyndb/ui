export interface RefreshConfig {
  AccessTokenRefreshSeconds: number
  RefreshTokenRefreshSeconds: number
}

export function LinkExist(links: any, name: string) {
  return links[name] !== undefined && links[name] !== ''
}

export class DynDBCore {}

export const core: DynDBCore = new DynDBCore()
