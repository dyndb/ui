import { useDocumentsStore } from '@/stores/documents'
import api from '.'
import { type DocumentStore, useDocumentStore } from '../stores/document'
import { errors } from './error'
import { type TemplateLayoutField } from './templates'

export type DocType = undefined | 'field' | 'template' | 'document' | 'attachment'

export type DocumentAction = "list" | "preview" | "view" | "collect" | "create" | "edit" | "delete" | "layout";

export interface Document {
  [template: string]: any
}

export interface JSONSingleDoc {

  // common fields
  displayText: string
  faicon: string | undefined

  // any other fields
  [template: string]: any

  // hal stuff
  _embedded?: Record<string, JSONSingleDoc> | undefined
  _links?: JSONSingleDocLinks | undefined

  // dyndb-specific stuff
  _dyndb_templatename?: string | undefined
  _dyndb_builtin?: boolean | undefined
  _dyndb_layout?: TemplateLayoutField[]
}

export interface JSONSingleDocLinks {
  self?: string | undefined
  save?: string | undefined
  delete?: string | undefined
  link?: string | undefined

  attmtlist?: string | undefined
  attmtupload?: string | undefined
}

export interface JSONSingleDocMetadata {
  builtin?: boolean | undefined
  templatename?: string | undefined
}

export class DynDBDoc {
  /**
   * Load an document from the backend without store it to the store
   * @param url
   * @param storeIt
   * @returns
   */
  public async Fetch(url: string | undefined): Promise<JSONSingleDoc> {
    if (url === undefined) { throw errors.ParameterUndefined }

    const response = await api.request.get(url, '')
    if (response.status !== 200) { throw errors.LoadError(response.data) }

    const document = response.data as JSONSingleDoc

    if (document?._dyndb_templatename === undefined) {
      throw errors.ValidationFailed('templatename missing')
    }

    const documentStore = useDocumentStore()
    documentStore.Loaded(document)
    return document
  }

  /**
   * Load an document from the backend and store it
   * @param selfURL
   * @returns
   */
  public async Load(selfURL: string | undefined): Promise<JSONSingleDoc> {

    // check
    if (selfURL === undefined) {
      throw errors.ValidationFailed('url missing')
    }

    // load it from backend
    const response = await api.request.get(selfURL, '')
    if (response.status !== 200) {
      throw errors.LoadError(response.data)
    }

    const document = response.data as JSONSingleDoc

    // validate result
    if (document?._dyndb_templatename === undefined) {
      throw errors.ValidationFailed('templatename missing')
    }

    const documentStore = useDocumentStore()
    documentStore.Loaded(document)
    return document
  }

  public DetectDocType(currentDocument: JSONSingleDoc | undefined): DocType {
    if (currentDocument === undefined) { return undefined }
    if (currentDocument._links?.self === undefined) { return undefined }

    let lastIndex = currentDocument._links.self.lastIndexOf('/')
    let elementID = currentDocument._links.self.substring(lastIndex + 1)

    if (elementID.startsWith('bmta-')) {
      return 'attachment'
    }

    let isDoc = elementID.startsWith('doc-')

    if (isDoc && ('tplName' in currentDocument || currentDocument._dyndb_templatename === 'dyndb_template')) {
      return 'template'
    }

    if (isDoc && 'fieldName' in currentDocument) {
      return 'field'
    }

    if (isDoc) {
      return 'document'
    }


    return undefined
  }

  public async Create(templateName: string | undefined): Promise<JSONSingleDoc> {
    const documentStore = useDocumentStore()

    // document
    if (templateName === undefined) {
      const err = errors.ValidationFailed('templateName is undefined')
      documentStore.Error(err)
      throw err
    }

    const documentToStore = JSON.parse(
      JSON.stringify({ _dyndb_templatename: templateName } as JSONSingleDoc)
    )

    // save it
    const response = await api.request.put("/document/-", documentToStore)
    if (response.status !== 200) {
      let err = errors.SaveError(response.data)
      documentStore.Error(err)
      throw err
    } else {
      documentStore.Saved(response.data)
    }

    return response.data as JSONSingleDoc
  }

  /**
   * Will save an document from the documents-store
   * @returns
   */
  public async Save(document: JSONSingleDoc | undefined): Promise<void> {
    const documentStore = useDocumentStore()

    if (document === undefined) {
      document = documentStore.$state.document
    }

    // document
    if (document === undefined) {
      const err = errors.ValidationFailed('document is undefined')
      documentStore.Error(err)
      throw err
    }

    // check
    if (document._links?.save === undefined) {
      const err = errors.ValidationFailed('save not possible')
      documentStore.Error(err)
      throw err
    }

    // copy it
    const documentToStore = JSON.parse(JSON.stringify(document))
    const documentLinks = document._links
    const documentTemplateName = document._dyndb_templatename

    // we keep _dyndb-fields, that are part of the document !
    // all others _ are artifical and added by the backend ! ( for example _links)
    for (const key in documentToStore) {
      if (key.startsWith('_dyndb')) continue
      if (key.startsWith('_')) {
        delete documentToStore[key]
      }
    }

    // save it
    const response = await api.request.put(document._links?.save, documentToStore)
    if (response.status !== 200) {
      const err = errors.SaveError(response.data)
      documentStore.Error(err)
      throw err
    }

    // remove it from cache
    if (documentStore.$state.links?.self !== undefined) {
      documentStore.$state.cached.delete(documentStore.$state.links.self)
    }

    documentStore.Saved(document)
    return
  }

  public async Link(document: JSONSingleDoc | undefined, linkToSelfURL: string | undefined): Promise<void> {
    const documentStore = useDocumentStore()

    // document
    if (document === undefined) {
      const err = errors.ValidationFailed('document is undefined')
      documentStore.Error(err)
      throw err
    }

    // check
    if (document._links?.link === undefined || linkToSelfURL === undefined) {
      const err = errors.ValidationFailed('link not possible')
      documentStore.Error(err)
      throw err
    }

    // link it
    const response = await api.request.post(document._links?.link, { _self: linkToSelfURL })
    if (response.status !== 200) {
      const err = errors.SaveError(response.data)
      documentStore.Error(err)
      throw err
    }

    return
  }

  public async Delete(document: JSONSingleDoc | undefined): Promise<void> {
    const documentStore = useDocumentStore()

    // document
    if (document === undefined) {
      const err = errors.ValidationFailed('document is undefined')
      documentStore.Error(err)
      throw err
    }

    // check
    if (document._links?.delete === undefined) {
      const err = errors.ValidationFailed('delete not possible')
      documentStore.Error(err)
      throw err
    }

    // delete it
    const response = await api.request.delete(document._links.delete, '')
    if (response.status !== 200) {
      const err = errors.DeleteError(response.data)
      documentStore.Error(err)
      throw err
    }

    documentStore.Deleted(document)
    return
  }

  public async Cancel() {
    const documentStore = useDocumentStore()

    // remove it from cache
    if (documentStore.$state.document?._links?.self !== undefined) {
      documentStore.$state.cached.delete(documentStore.$state.document._links.self)
    }
  }

  // Layout will read all keys of the current document and create layoutfields from it
  public Layout(): TemplateLayoutField[] | undefined {
    // add all from keys
    const documentStore = useDocumentStore()
    const documentLayout = documentStore.$state.document as JSONSingleDoc

    return documentLayout._dyndb_layout
  }

  public LayoutFromDocFields(doc: JSONSingleDoc | undefined): TemplateLayoutField[] {
    if (doc === undefined) return []

    const fieldMap = new Map<string, TemplateLayoutField>()

    // add all from keys
    for (const docKey of Object.keys(doc)) {
      switch (docKey) {
        case '_template':
        case '_metadata':
        case '_links':
        case '_dyndb_layout':
          break
        default:
          fieldMap.set(docKey, {
            fieldName: docKey
          } as TemplateLayoutField)
      }
    }

    return Array.from(fieldMap.values())
  }

  public LayoutSet(fields: TemplateLayoutField[]) {
    const documentStore = useDocumentStore()
    const document = documentStore.$state.document
    if (document !== undefined) {
      document._dyndb_layout = fields
    }
  }
}
