import api from '.'
import { useDocumentsStore } from '../stores/documents'
import { IsError, errors } from './error'
import { type JSONSingleDoc } from './document'
import { useDocumentStore } from '../stores/document'
import { type AxiosResponse } from 'axios'

export interface JSONPagedDoc {
  [template: string]: any
  _embedded?: Record<string, JSONSingleDoc> | undefined
  _links?: JSONPagedDocLinks | undefined
}

export interface JSONPagedDocLinks {
  self?: string | undefined
  create?: string | undefined
  first?: string | undefined
  prev?: string | undefined
  next?: string | undefined
  search?: string | undefined
}

// DynDBPagedDocuments holds a store and can fetch documents to it
export class Documents {
  /**
   * Fetch documents from an url
   *
   *
   * @param InOut.url The url from where we fetch the documents, if its undefined errors.ParameterUndefined will be returned
   * @param InOut.payload The payload as string or object. If this is set, POST will be used
   * @param InOut.documents Here the documents will be saved
   * @param InOut.links Here the links will be saved
   * @returns
   */
  public async FetchFromURL(InOut: {
    url: string | undefined
    payload?: string | Object | undefined
    documents: Record<string, JSONSingleDoc>
    links: JSONPagedDocLinks
  }): Promise<Error | null> {
    if (InOut.url === undefined) return errors.ParameterUndefined

    // request
    let response: AxiosResponse<any, any>
    if (InOut.payload === undefined) {
      response = await api.request.get(InOut.url, '')
    } else {
      response = await api.request.post(InOut.url, InOut.payload)
    }

    if (response.status !== 200) {
      return errors.LoadError(response.data)
    }

    const document = response.data as JSONPagedDoc
    if (document._embedded === undefined) {
      return errors.NoDocument
    }
    if (document._links === undefined) {
      return errors.NoDocument
    }

    // get template Name
    /*
    const templateName = Object.keys(document._embedded)[0]
    const embeddedDocs = document._embedded[templateName] as Record<string, JSONSingleDoc>
    */
    InOut.documents = { ...InOut.documents, ...document._embedded }
    InOut.links = document._links

    return null
  }

  /**
   * This fetch all full documents from the backend.
   *
   * This function not store anything in the pinia-store
   * @param Promise with an array of fetched documents
   */
  public async FetchAll(templateName: string): Promise<Record<string, JSONSingleDoc>> {
    const documents: Record<string, JSONSingleDoc> = {}
    const links: JSONPagedDocLinks = {}

    // request
    const FetchFromURLOptions = {
      url: `/documents/list/full/${templateName}/0/5` as string | undefined,
      documents: documents,
      links: links
    }
    const err = await this.FetchFromURL(FetchFromURLOptions)

    // request - next
    if (err === null) {
      FetchFromURLOptions.url = FetchFromURLOptions.links.next
      while (FetchFromURLOptions.url !== undefined) {
        await this.FetchFromURL(FetchFromURLOptions)
        FetchFromURLOptions.url = FetchFromURLOptions.links.next
      }
    }

    //
    return FetchFromURLOptions.documents
  }

  /**
   * This fetch the first documents as short list and store it to pinia-store
   *
   * @param templateName
   * @param pageSize
   */
  public async ListFirst(templateName: string, pageSize: number) {
    // store
    const documentsStore = useDocumentsStore()

    // request
    const FetchFromURLOptions = {
      url: `/documents/list/short/${templateName}/0/${pageSize}`,
      documents: {},
      links: {}
    }
    const err = await this.FetchFromURL(FetchFromURLOptions)

    if (err === null) {
      documentsStore.Set(FetchFromURLOptions.documents)
      documentsStore.links = FetchFromURLOptions.links
    } else if (err === errors.NoDocument) {
      documentsStore.Set({})
      documentsStore.links = {}
    }
  }

  public async ListNext() {
    // store
    const documentsStore = useDocumentsStore()

    // request
    const FetchFromURLOptions = {
      url: documentsStore.links?.next,
      documents: {},
      links: {}
    }
    const err = await this.FetchFromURL(FetchFromURLOptions)

    if (err === null) {
      documentsStore.Set(FetchFromURLOptions.documents)
      documentsStore.links = FetchFromURLOptions.links
    }
  }

  public async ListPrev() {
    // store
    const documentsStore = useDocumentsStore()

    // request
    const FetchFromURLOptions = {
      url: documentsStore.links?.prev,
      documents: {},
      links: {}
    }
    const err = await this.FetchFromURL(FetchFromURLOptions)

    if (err === null) {
      documentsStore.Set(FetchFromURLOptions.documents)
      documentsStore.links = FetchFromURLOptions.links
    }
  }

  public async Refresh() {
    // store
    const documentsStore = useDocumentsStore()

    // request
    const FetchFromURLOptions = {
      url: documentsStore.links?.self,
      documents: {},
      links: {}
    }
    const err = await this.FetchFromURL(FetchFromURLOptions)

    if (err === null) {
      documentsStore.Set(FetchFromURLOptions.documents)
      documentsStore.links = FetchFromURLOptions.links
    }
  }

  public async Links(): Promise<JSONPagedDocLinks | undefined> {
    // store
    const documentsStore = useDocumentsStore()
    return documentsStore.$state.links
  }
}
