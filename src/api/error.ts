export interface Success {
  title: string
  detail?: string
  payload?: any
}

export const success = {
  Message: (
    title: string,
    detail: string | undefined = undefined,
    payload: any | undefined = undefined
  ) => {
    return { title, detail, payload } as Success
  }
}

export const errors = {
  LoadError: (message: string) => {
    return { name: 'load error', message: message } as Error
  },
  SaveError: (message: string) => {
    return { name: 'save error', message: message } as Error
  },
  DeleteError: (message: string) => {
    return { name: 'delete error', message: message } as Error
  },

  PreconditionFailed: (message: string) => {
    return { name: 'precondition error', message: message } as Error
  },
  ValidationFailed: (message: string) => {
    return { name: 'precondition error', message: message } as Error
  },

  ParameterUndefined: { name: 'programming error', message: 'needed parameter undefined' } as Error,

  NoDocument: { name: 'no document', message: 'there is no document' } as Error,
  SaveNotPossible: {
    name: 'save error',
    message: 'you try to save an object that can not be saved'
  } as Error,
  DeleteNotPossible: {
    name: 'save error',
    message: 'you try to save an object that can not be saved'
  } as Error,
  UploadNotPossible: { name: 'attachment error', message: 'can not upload a document' } as Error,
  AttachmentNotPossible: {
    name: 'attachment error',
    message: 'attachments for this document are not possible'
  } as Error,
  CreateNotPossible: { name: 'create error', message: 'can not create a new document' } as Error,
  NextNotPossible: { name: 'next error', message: 'can not request next documents' } as Error,

  SaveFailed: { name: 'save.failed.title', message: 'save.failed.detail' } as Error
}

export function IsSuccess(object: any): object is Success {
  if (typeof object === 'object') {
    return 'title' in object
  }
  return false
}

export function IsError(object: any): object is Error {
  if (typeof object === 'object') {
    return 'message' in object
  }
  return false
}
