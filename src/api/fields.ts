import { ScriptElementKind } from 'typescript'
import api from '.'
import { useFieldsStore } from '../stores/fields'
import { type DocType, type JSONSingleDoc } from './document'
import type { JSONPagedDoc } from './documents'
import { errors } from './error'

export interface Field extends JSONSingleDoc {
  fieldName: string
  displayText: string
  hint: string
  default: string
  datatype: string
  unit: string
}

export class DynDBFields {
  /**
   * Fetch all fields from backend if needed
   *
   * @returns
   */
  public async FieldsByName(fieldNames: string[]): Promise<Error | Field[]> {
    const fields: Field[] = []
    const fieldsMissing: string[] = []

    // store
    const fieldsStore = useFieldsStore()

    // check if field exist
    for (const fieldName of fieldNames) {
      const curField = fieldsStore.Get(fieldName)
      if (curField !== undefined && curField !== null) {
        fields.push(curField as Field)
      } else {
        fieldsMissing.push(fieldName)
      }
    }

    // request + store missing fields
    let document: JSONPagedDoc = {} as JSONPagedDoc
    if (fieldsMissing.length > 0) {
      const response = await api.request.post('/fields/byname', { fieldnames: fieldsMissing })
      document = response.data as JSONPagedDoc
      if (document._embedded === undefined) {
        return errors.NoDocument
      }
    }

    const loadedFields: Record<string, JSONSingleDoc> = {}
    if (document._embedded !== undefined) {
      Object.values(document._embedded).forEach(
        (value: JSONSingleDoc, index: number, array: JSONSingleDoc[]) => {
          loadedFields[(value as Field).fieldName] = value as Field
        }
      )
    }

    fieldsStore.Append(loadedFields)

    // get the rest
    for (const fieldName in fieldsMissing) {
      const curField = fieldsStore.Get(fieldName)
      if (curField !== undefined && curField !== null) {
        fields.push(curField as Field)
      }
    }

    const plainFields = Object.assign({}, fields)
    return plainFields
  }

  public async FetchAll() {
    const fieldsStore = useFieldsStore()

    // already there
    if (fieldsStore.documents.length > 0) {
      return fieldsStore.documents as Field[]
    }

    // fetch all
    const allFields = await api.documents.FetchAll('dyndb_field')

    const loadedFields: Record<string, JSONSingleDoc> = {}
    if (allFields !== undefined) {
      Object.values(allFields).forEach(
        (value: JSONSingleDoc, index: number, array: JSONSingleDoc[]) => {
          loadedFields[(value as Field).fieldName] = value as Field
        }
      )
    }

    fieldsStore.Append(loadedFields)
  }




  /**
   * Return of all Fields
   *
   * It request all fields if not done it before.
   * @returns Array of Field
   */
  public async Fields(): Promise<Field[]> {
    const fieldsStore = useFieldsStore()
    return fieldsStore.documents as Field[]
  }

  public async Append(newField: Field) {
    // validate
    if (newField.fieldName === undefined) {
      throw 'name of template missing'
    }

    const tmpRecord: Record<string, Field> = {}
    tmpRecord[newField.fieldName] = newField

    const templateStore = useFieldsStore()
    templateStore.Append(tmpRecord)
  }

  public async Remove(newField: Field) {
    // validate
    if (newField.fieldName === undefined) {
      throw 'name of template missing'
    }

    const templateStore = useFieldsStore()
    templateStore.Remove(newField.fieldName)
  }

  public async Field(fieldName: string): Promise<Field | undefined> {
    await this.Fields()

    const fieldsStore = useFieldsStore()

    const field: Field | undefined = fieldsStore.Get(fieldName) as Field | undefined
    return field
  }

  public async FromArray(fieldNames: string[]): Promise<Field[]> {
    await this.Fields()

    const fieldsStore = useFieldsStore()
    const fields: Field[] = []

    for (const fieldName of fieldNames) {
      const field = fieldsStore.Get(fieldName)
      if (field !== undefined) {
        fields.push(field as Field)
      }
    }

    console.debug('loaded fields', fieldNames, fields)

    return fields
  }

  // get the fields from doc
  public async FromDoc(doc: any): Promise<Field[]> {
    await this.Fields()

    if (doc === undefined) {
      console.warn('document is undefined, returning empty field-array')
      return []
    }

    // get all fields from doc
    const docFields = Object.keys(doc)

    console.debug('fieldnames', docFields)
    return this.FromArray(docFields)
  }
}
