

const nativeComponents = new Map<string, any>();

export async function RegisterNativeComponent(name: string, component: any): Promise<void> {
  if (nativeComponents.has(name)) { return }
  nativeComponents.set(name, component)
}

export function GetNativeComponent(name: any | undefined) {
  if (name === undefined) { return undefined }

  if (typeof name === 'object') {
    return name
  }

  if (typeof name !== 'string') {
    console.error('name is not string, its:', typeof name)
    return
  }

  return nativeComponents.get(name)
}