import { TokenHandler, type TokenHandlerOpts } from './tokens'
import { DynDBAttachment } from './attachment'
import { DynDBCore } from './core'
import { DynDBDoc } from './document'
import { DynDBFields } from './fields'
import { DynDBNotifications } from './notifications'
import { DynDBRequests } from './requests'
import { DynDBTemplates } from './templates'
import { dynDBSearch } from './search'
import { DynDBDocLinks } from './links'
import { DynDBCollector } from './collector'
import { DynDBAuth } from './auth'
import { DynDBWindows } from './windows'
import { Documents } from './documents'
import { SSE } from './sse'

class DynDB {
  public request: DynDBRequests
  public tokenHandler: TokenHandler
  public core: DynDBCore = new DynDBCore()
  public auth = new DynDBAuth()

  // ui stuff
  public windows = new DynDBWindows()
  public notifications: DynDBNotifications = new DynDBNotifications()
  public sse: SSE = new SSE()

  // backend stuff
  public document: DynDBDoc = new DynDBDoc()
  public documents: Documents = new Documents()
  public search: dynDBSearch = new dynDBSearch()
  public links: DynDBDocLinks = new DynDBDocLinks()
  public attachments: DynDBAttachment = new DynDBAttachment()

  public fields: DynDBFields = new DynDBFields()
  public templates: DynDBTemplates = new DynDBTemplates()
  public collection: DynDBCollector = new DynDBCollector()

  constructor() {
    // setup requests
    this.request = new DynDBRequests()

    // setup token interceptors
    this.tokenHandler = new TokenHandler({
      localStorageKey: 'dyndb_refresh_token',
      RefreshTokenCookieName: 'dyndb-token-refresh',
      AccessTokenCookieName: 'dyndb-token-access',
      tokenRefreshUrl: '/me'
    } as TokenHandlerOpts)
    this.tokenHandler.SetupTokenInterceptors(this.request.ax)
    this.notifications.SetupTokenInterceptors(this.request.ax)
  }
}
const api = new DynDB()
export default api
