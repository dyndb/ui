import api from '.'
import { useDocLinksStore } from '../stores/links'
import { errors } from './error'

export interface ToLinkDocuments {
  srcSelfLink: string
  dstSelfLink: string
}

// list of links
export interface DocLinksElements {
  _embedded?: { [docid: string]: DocLinksElement } | undefined
  _links?: DocLinksElementsLinks | undefined
}

export interface DocLinksElementsLinks {
  doclinkslist?: string
}

// single link
export interface DocLinksElement {
  templateName: string
  displayName: string
  _links?: DocLinksElementLinks | undefined
}

export interface DocLinksElementLinks {
  self?: string
  doclinksadd?: string
  doclinkremove?: string
}

export class DynDBDocLinks {
  public async FetchAll(url: string | undefined): Promise<DocLinksElements | Error> {
    if (url === undefined) return errors.ParameterUndefined

    const response = await api.request.get(url, '')
    if (response.status !== 200) return errors.LoadError(response.data)

    const responseData = response.data as DocLinksElements
    const responseLinks = responseData._links as DocLinksElementsLinks | undefined

    const doclinks = new Map<string, DocLinksElement>()
    if (responseData._embedded !== undefined) {
      for (const docid in responseData._embedded) {
        doclinks.set(docid, responseData._embedded[docid])
      }
    }

    const docLinksStore = useDocLinksStore()
    /*
    docLinksStore.$patch({
      doclinks: doclinks,
      links: responseLinks
    })
    */

    return responseData
  }

  public async Refresh() {
    const docLinksStore = useDocLinksStore()
    if (docLinksStore.links?.doclinkslist !== undefined) {
      this.FetchAll(docLinksStore.links.doclinkslist)
    }
  }

  public async LinkToDoc(opts: {
    addLinkURL: string | undefined
    docSelfURL: string | undefined
  }): Promise<true | Error> {
    if (opts.addLinkURL === undefined || opts.docSelfURL === undefined) {
      return errors.ParameterUndefined
    }

    const response = await api.request.post(opts.addLinkURL, opts.docSelfURL)
    if (response.status !== 200) return errors.SaveFailed

    return true
  }

  public async UnLink(url: string): Promise<true | Error> {
    const response = await api.request.delete(url, '')
    if (response.status !== 200) return errors.SaveFailed

    return true
  }
}
