import type { AxiosInstance, AxiosResponse } from 'axios'
import { useNotificationStore } from '../stores/notification'

export class DynDBNotifications {
  public constructor() { }


  public SetupTokenInterceptors(ax: AxiosInstance | undefined = undefined) {
    if (ax === undefined) {
      throw new Error('ax is undefined')
    }

    // handle incoming refresh-tokens
    ax.interceptors.response.use(
      async (response: AxiosResponse) => {
        return Promise.resolve(response)
      },
      (response: AxiosResponse) => {
        const notificationStore = useNotificationStore()
        if (response.status === 403) {
          this.Error("You are not allowed to do this")
        }
        if (response.status === 500) {
          this.Error("An error occured", response.data)
        }
        return Promise.reject(response)
      }
    )
  }

  Show(color: string, message: string, detail?: string) {
    const notificationStore = useNotificationStore()
    notificationStore.Show(color, message, detail)
  }

  Sucess(message: string) {
    this.Show('positive', message, '')
  }
  Error(message: string, detail?: string) {
    this.Show('negative', message, detail)
  }
}
