import axios, {
  type AxiosInstance,
  type AxiosRequestConfig,
  type AxiosResponse,
  isAxiosError,
  type RawAxiosRequestConfig
} from 'axios'

import { useLoginStore } from '../stores/login'
import { TokenHandler, type TokenHandlerOpts } from './tokens'

export class DynDBRequests {
  private axConfig: RawAxiosRequestConfig = {
    baseURL: '/api/v1'
    //baseURL: 'http://localhost:3535/api/v1'
  }
  public ax: AxiosInstance
  private readonly: boolean = false

  constructor() {
    this.ax = axios.create(this.axConfig)

    this.ax.interceptors.response.use(
      function (response: AxiosResponse) {
        if (response.status === 401) {
          const store = useLoginStore()
          store.$patch({ loggedin: false })
        }
        return response
      },
      function (error) {
        if (isAxiosError(error)) {
          if (error.response?.status === 401) {
            const store = useLoginStore()
            store.$patch({ loggedin: false })
          }
          return Promise.reject(error.response)
        }

        return Promise.reject(error)
      }
    )
  }

  public BaseURL(): string | undefined {
    return this.axConfig.baseURL
  }

  public setBaseURL(url: string) {
    this.axConfig.baseURL = url
    this.ax = axios.create(this.axConfig)
  }

  private async rawrequest(
    method: string = 'GET',
    path: string,
    payload: any = ''
  ): Promise<AxiosResponse> {
    // FormData
    let headers: any | undefined = undefined

    if (payload instanceof FormData) {
      headers = {
        'Content-Type': 'multipart/form-data'
      }
    } else {
      // marshall to json
      if (typeof payload === 'object') {
        payload = JSON.stringify(payload)
        headers = {
          'Content-Type': 'application/json'
        }
      }
    }

    console.debug(method, path, payload)

    return this.ax.request({
      url: path,
      method: method,
      data: payload,
      headers: headers
    } as AxiosRequestConfig)
  }

  public ReadOnly(): boolean {
    return this.readonly
  }

  public SetReadOnly(ro: boolean) {
    this.readonly = ro
  }

  public async head(path: string, payload: string | object) {
    return this.rawrequest('HEAD', path, payload)
  }

  public async get(path: string, payload: string | object) {
    return this.rawrequest('GET', path, payload)
  }

  public async post(path: string, payload: string | object): Promise<AxiosResponse> {
    if (this.readonly === true) {
      return {
        status: 200
      } as AxiosResponse
    }
    return this.rawrequest('POST', path, payload)
  }

  public async put(path: string, payload: string | object): Promise<AxiosResponse> {
    if (this.readonly === true) {
      return {
        status: 200
      } as AxiosResponse
    }
    return this.rawrequest('PUT', path, payload)
  }

  public async delete(path: string, payload: string | object): Promise<AxiosResponse> {
    if (this.readonly === true) {
      return {
        status: 200
      } as AxiosResponse
    }
    return this.rawrequest('DELETE', path, payload)
  }

  public async download(path: string, filename: string) {
    this.ax({
      url: path,
      method: 'GET',
      responseType: 'blob' // important
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', filename) //or any other extension
      document.body.appendChild(link)
      link.click()
    })
  }

  public async downloadFromURL(path: string | undefined) {
    if (path === undefined) return

    this.ax({
      url: path,
      method: 'GET',
      responseType: 'blob' // important
    }).then((response) => {
      const fileNameHeader = 'x-suggested-filename'
      const suggestedFileName = response.headers[fileNameHeader]
      const effectiveFileName = suggestedFileName === undefined ? 'unknown' : suggestedFileName

      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', effectiveFileName) //or any other extension
      document.body.appendChild(link)
      link.click()
    })
  }
}
