import api from '.'
import { useDocumentsStore } from '../stores/documents'

export interface dynDBSearchRequest {
  words: string[]
}

export class dynDBSearch {
  /**
   * Will request an search on the backend and update the documents-store.
   * @param words Searching for these words
   * @returns
   */
  public async Search(words: string[]) {
    const documentsStore = useDocumentsStore()
    if (words.length === 0) return

    // if its empty
    if (words.length === 1) {
      if (words[0] === '') {
        documentsStore.$reset()
        return
      }
    }

    const FetchFromURLOptions = {
      url: '/documents/search/short/0/25',
      payload: {
        words: words
      } as dynDBSearchRequest,
      documents: {},
      links: {}
    }

    const err = await api.documents.FetchFromURL(FetchFromURLOptions)

    if (err === null) {
      documentsStore.Append(FetchFromURLOptions.documents)
      documentsStore.links = FetchFromURLOptions.links
    }

  }
}
