import { type MessagingData, useMessagingStore } from '../stores/messaging'
import { useNotificationStore } from '../stores/notification'

export interface SSEMessage {
  t: string // Topic
  n: number // Type
  v: string // Value
}

export class SSE {
  private evtSource: EventSource | undefined

  async Connect() {
    const messagingStore = useMessagingStore()

    if (messagingStore.$state.connected) {
      console.warn('already connected')
      return
    }

    this.evtSource = new EventSource('/api/v1/events')
    this.evtSource.addEventListener('message', function (e) {
      // inform the store that we are connected
      if (messagingStore.$state.connected === false) {
        messagingStore.$patch({
          connected: true
        })
      }

      let data: SSEMessage = {} as SSEMessage

      // parse message
      try {
        data = JSON.parse(e.data) as SSEMessage
      } catch (e) {
        const notificationStore = useNotificationStore()
        notificationStore.Show('red', 'JSON-Error', e as any)
        return
      }

      // try to parse payload as json
      let payload: string | Object = ''
      try {
        payload = JSON.parse(data.v)
      } catch (e) {
        payload = data.v
      }

      // store it
      // messagingStore.Set(data.t, payload);
      messagingStore.$state.message = {
        topic: data.t,
        payload: payload
      } as MessagingData
    })

    this.evtSource.onerror = (ev: Event) => {
      console.error('EventSource failed:', ev)

      messagingStore.$patch({
        connected: false
      })
    }
  }

  Disconnect() {
    const messagingStore = useMessagingStore()

    this.evtSource?.close()
    this.evtSource = undefined
    messagingStore.$patch({
      connected: false
    })
  }

  IsConnected(): boolean {
    const messagingStore = useMessagingStore()

    return messagingStore.$state.connected
  }
}

const $sse = new SSE()
export default $sse
