import api from '.'
import { useTemplatesStore } from '../stores/templates'
import { type JSONSingleDoc } from './document'
import { IsError, errors } from './error'

export interface Template extends JSONSingleDoc {
  tplName: string
  description: string
}

export interface TemplateLayoutField {
  fieldName: string
  x?: number
  y?: number
  colspan?: number
  rowspan?: number
}

export class DynDBTemplates {
  public async FetchAll() {
    const templateStore = useTemplatesStore()

    // already there
    if (templateStore.documents.length > 0) {
      return templateStore.documents as Template[]
    }

    // fetch all
    const allTemplates = await api.documents.FetchAll('dyndb_template')

    const loadedTemplates: Record<string, JSONSingleDoc> = {}
    if (allTemplates !== undefined) {
      Object.values(allTemplates).forEach(
        (value: JSONSingleDoc, index: number, array: JSONSingleDoc[]) => {
          loadedTemplates[(value as Template).tplName] = value as Template
        }
      )
    }

    templateStore.Append(loadedTemplates)
  }

  public async Templates(): Promise<Template[]> {
    await this.FetchAll()

    const templateStore = useTemplatesStore()
    return templateStore.documents as Template[]
  }

  /**
   * Append the newTemplate to the store
   * 
   * Throws an error if the template-name is undefined
   * @param newTemplate 
   */
  public async Append(newTemplate: Template) {
    // validate
    if (newTemplate.tplName === undefined) {
      throw 'name of template missing'
    }

    const tmpRecord: Record<string, Template> = {}
    tmpRecord[newTemplate.tplName] = newTemplate

    const templateStore = useTemplatesStore()
    templateStore.Append(tmpRecord)
  }

  public async Remove(newTemplate: Template) {
    // validate
    if (newTemplate.tplName === undefined) {
      throw 'name of template missing'
    }

    const templateStore = useTemplatesStore()
    templateStore.Remove(newTemplate.tplName)
  }

  public async Template(name: string | undefined): Promise<Template | Error> {
    if (name === undefined) return errors.ParameterUndefined
    await this.Templates()

    const templateStore = useTemplatesStore()

    const template = templateStore.Get(name)
    if (template === undefined) return errors.LoadError('')

    return template as Template
  }

  public async Layout(name: string | undefined): Promise<TemplateLayoutField[] | Error> {
    if (name === undefined) return []

    const template = await this.Template(name)
    if (IsError(template)) {
      return template
    }

    if (template._dyndb_layout === undefined) {
      return []
    }

    return template._dyndb_layout
  }
}
