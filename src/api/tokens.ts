import { type AxiosInstance, type AxiosResponse } from 'axios'
import { deleteCookie, getCookie, setCookie } from './cookies'
import { useTokenStore } from '../stores/tokens'
import { type JwtPayload, jwtDecode } from 'jwt-decode'

export interface AccessTokenClaims {
  refresh_in_seconds: number | undefined
}

export interface TokenHandlerOpts {
  RefreshTokenCookieName: string | undefined
  AccessTokenCookieName: string | undefined
  tokenRefreshUrl: string | undefined
}

export class TokenHandler {
  private RefreshTokenCookieName = ''
  private AccessTokenCookieName = ''
  private tokenRefreshUrl: string = ''

  constructor(opts: TokenHandlerOpts) {
    if (opts.RefreshTokenCookieName === undefined) {
      opts.RefreshTokenCookieName = 'app-token-refresh'
    }
    if (opts.AccessTokenCookieName === undefined) {
      opts.AccessTokenCookieName = 'app-token-access'
    }
    if (opts.tokenRefreshUrl === undefined) {
      opts.tokenRefreshUrl = '/me'
    }

    // copy opts
    this.RefreshTokenCookieName = opts.RefreshTokenCookieName
    this.AccessTokenCookieName = opts.AccessTokenCookieName
    this.tokenRefreshUrl = opts.tokenRefreshUrl
  }



  public SetupTokenInterceptors(ax: AxiosInstance | undefined = undefined) {
    if (ax === undefined) {
      throw new Error('ax is undefined')
    }

    // handle incoming refresh-tokens
    ax.interceptors.response.use(
      async (response: AxiosResponse) => {
        const tokenStore = useTokenStore()

        // get the token from request
        const refreshToken = await this.RefreshTokenExtract()
        if (refreshToken !== undefined) {
          tokenStore.RefreshTokenSet(refreshToken)
        }

        // ensure that the timer is running
        if (tokenStore.RefreshTokenTimer === undefined) {
          tokenStore.RefreshTokenOnTimeout(async (currentToken: string) => {
            this.RefreshTokenInject(currentToken)
            return
          })
        }



        return Promise.resolve(response)
      },
      (response: AxiosResponse) => {
        if (response.status === 401) {
          this.InvalidateTokens()
        }
        return Promise.reject(response)
      }
    )
  }

  // ####################### Access token #######################
  public async AcessTokenExist(): Promise<boolean> {
    return (await getCookie(this.AccessTokenCookieName)) !== undefined
  }

  public async AcessTokenExtract(): Promise<string | undefined> {
    const cookieValue = await getCookie(this.AccessTokenCookieName)
    return cookieValue
  }

  public async AccessTokenExpire(): Promise<Date | undefined> {
    const accessToken = await this.AcessTokenExtract()
    if (accessToken === undefined) return undefined

    const decoded = jwtDecode<JwtPayload>(accessToken)
    if (decoded.exp !== undefined) {
      return new Date(decoded.exp * 1000)
    }

    return undefined
  }

  // ####################### Refresh token #######################
  public async RefreshTokenExist(): Promise<boolean> {
    return (await getCookie(this.RefreshTokenCookieName)) !== undefined
  }

  private async RefreshTokenExtract() {
    const cookieValue = await getCookie(this.RefreshTokenCookieName)
    await deleteCookie(this.RefreshTokenCookieName)
    return cookieValue
  }

  public async RefreshTokenExpire(): Promise<Date | undefined> {
    const tokenStore = useTokenStore()

    if (tokenStore.RefreshToken === undefined) { return undefined }
    const decoded = jwtDecode<JwtPayload>(tokenStore.RefreshToken)
    if (decoded.exp !== undefined) {
      return new Date(decoded.exp * 1000)
    }

    return undefined
  }

  public async RefreshTokenInject(refreshToken: string) {
    setCookie(this.RefreshTokenCookieName, refreshToken)
    console.debug('RefreshToken: refresh token with the next request')
  }

  // ####################### Access token #######################

  public async InvalidateTokens() {

    const tokenStore = useTokenStore()
    tokenStore.RefreshToken = undefined

    deleteCookie(this.RefreshTokenCookieName)
    deleteCookie(this.AccessTokenCookieName)

    console.debug('invalidate tokens')
  }
}
