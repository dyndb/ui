import { v4 as uuidv4 } from 'uuid'
import { useWindowStore } from '../stores/windows'

export interface DynWindow {
  ID: string
  icon: string
  displayName: string
  subtitle?: string

  prepare?: (win: DynWindow) => Promise<void>

  uri?: string | (() => Promise<string>)
  params?: { [template: string]: string } | undefined

  component?: any
  componentParameter?: any

  // can not be written
  childs?: Record<string, DynWindow>

  // state ( can not be overwritten )
  closable?: boolean

  // hidden will not show this window
  // this is mainly for windows that are for grouping
  // like "overlay" window-group
  hidden?: boolean

  // if set to true, create a new instance of this window with an
  // random id
  createInstance: boolean

  // index represent the number
  index: number

  // you can store a state
  state?: any
}

export class DynDBWindows {
  public constructor() { }

  // SetWindow path an already existing window or create new if not exist
  // also if the parent not exist, it will be created
  public async SetWindow(parentID: string, win: DynWindow): Promise<string> {
    const windowStore = useWindowStore()

    // closable default
    if (win.closable === undefined) {
      win.closable = false
    }

    // autogen id
    if (win.ID === '' || win.ID === undefined) {
      win.ID = uuidv4()
    } else {
      // find window
      const curWindow = windowStore.FindWindow(win.ID)

      // patch it
      if (curWindow !== undefined) {
        if (win.icon !== undefined) curWindow.icon = win.icon
        if (win.displayName !== undefined) curWindow.displayName = win.displayName
        if (win.subtitle !== undefined) curWindow.subtitle = win.subtitle
        if (win.uri !== undefined) curWindow.uri = win.uri
        if (win.params !== undefined) curWindow.params = win.params

        return win.ID
      }
    }

    // ensure vars
    if (win.childs === undefined) {
      win.childs = {}
    }

    // save it
    windowStore.Save(parentID, win)

    return win.ID
  }

  public async SetWindowActive(id: string | undefined) {
    if (id === undefined) {
      console.debug('id missing, can not set window to active')
      return
    }

    const windowStore = useWindowStore()
    windowStore.SetActive(id)
  }

  public async RouterURL(id: string): Promise<string | undefined> {
    const windowStore = useWindowStore()

    const window = windowStore.FindWindow(id) as DynWindow | undefined
    if (window !== undefined) {
      if (typeof window.uri === 'string') {
        return window.uri
      }
      if (typeof window.uri === 'function') {
        const newRoute = await window.uri()
        if (newRoute === '') {
          return '/'
        }
        return newRoute
      }
    }

    return undefined
  }

  public async Close(id: string) {
    const windowStore = useWindowStore()

    if (id === windowStore.$state.windowActive?.ID) {
      windowStore.$state.windowActive = undefined
    }

    windowStore.RemoveWindow(id)
  }

  public async CloseCurrent() {
    const windowStore = useWindowStore()
    if (windowStore.$state.windowActive?.ID !== undefined) {
      this.Close(windowStore.$state.windowActive?.ID)
    }
  }
}
