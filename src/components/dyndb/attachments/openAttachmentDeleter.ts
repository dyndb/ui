

import api from "@/api"
import type { DynDBAttachmentLinks } from "@/api/attachment"
import { errors } from "@/api/error"
import type { DynWindow } from "@/api/windows"
import { useNotificationStore } from "@/stores/notification"
import { fasFilePdf } from "@quasar/extras/fontawesome-v6"

// components
import { RegisterNativeComponent } from "@/api/globalComponents"
import dyndbPreviewDelete from '@/components/dyndb/attachments/dyndb-preview-delete.vue'
RegisterNativeComponent('dyndb-preview-delete', dyndbPreviewDelete)

export async function openAttachmentDeleter(mode: 'view' | 'preview', selfURL: string | undefined) {
    if (selfURL === undefined) { return }


    const notificationStore = useNotificationStore()

    const loadedDocument = await api.document.Fetch(selfURL)
    const loadedDocumentType = api.document.DetectDocType(loadedDocument)

    if (loadedDocument === undefined) {
        notificationStore.ShowError(errors.PreconditionFailed('loadedDocument is undefined'))
        return
    }

    if (loadedDocumentType === 'attachment') {
        api.windows.SetWindow(mode === 'view' ? 'overlayed' : 'dynamic', {
            ID: (loadedDocument._links as DynDBAttachmentLinks).preview,
            icon: fasFilePdf,
            displayName: loadedDocument.displayText,
            closable: true,

            component: 'dyndb-preview-delete',
            componentParameter: {
                metadata: loadedDocument,
            },
        } as DynWindow)

        api.windows.SetWindowActive((loadedDocument._links as DynDBAttachmentLinks).preview)

    }

}
