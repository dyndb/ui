

import api from "@/api"
import type { DynDBAttachmentLinks } from "@/api/attachment"
import { errors } from "@/api/error"
import type { DynWindow } from "@/api/windows"
import { useNotificationStore } from "@/stores/notification"
import { fasFilePdf } from "@quasar/extras/fontawesome-v6"

// components
import { RegisterNativeComponent } from "@/api/globalComponents"
import dyndbPreview from '@/components/dyndb/attachments/dyndb-preview.vue'
RegisterNativeComponent('dyndb-preview', dyndbPreview)

export async function openAttachmentPreviewer(mode: 'view' | 'preview', selfURL: string | undefined) {
    if (selfURL === undefined) { return }


    const notificationStore = useNotificationStore()

    const loadedDocument = await api.attachments.Fetch(selfURL)

    if (loadedDocument === undefined) {
        notificationStore.ShowError(errors.PreconditionFailed('loadedDocument is undefined'))
        return
    }

    api.windows.SetWindow(mode === 'view' ? 'dynamic' : 'overlayed', {
        ID: (loadedDocument._links as DynDBAttachmentLinks).preview,
        icon: fasFilePdf,
        displayName: loadedDocument.displayText,
        closable: true,

        component: 'dyndb-preview',
        componentParameter: {
            attachmentMetadata: loadedDocument,
            //url: 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf',
        },
    } as DynWindow)

    api.windows.SetWindowActive((loadedDocument._links as DynDBAttachmentLinks).preview)


}
