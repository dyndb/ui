import api from "@/api";
import type { DynDBAttachmentLinks } from "@/api/attachment";
import type { JSONSingleDoc } from "@/api/document";
import { useButtonbarStore } from "@/stores/buttonbar";
import { onMounted, watch, type Ref } from "vue";

// components
import { openAttachmentPreviewer } from "./openAttachmentPreviewer";
import { useNotificationStore } from "@/stores/notification";
import { openAttachmentDeleter } from "./openAttachmentDeleter";

export function useAttachmentActions(
    stackName: string,
    curDocument: Ref<JSONSingleDoc | undefined>,
) {

    const notificationStore = useNotificationStore()

    async function SetupButtons(curDocument: JSONSingleDoc | undefined) {
        if (curDocument === undefined) { return }

        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        // preview
        buttonBar.AddButton({
            id: 'preview',
            type: 'toolbar',
            position: 'left',
            displayText: 'Preview',
            icon: 'fasFilePdf',
            color: 'primary',
            action: () => {
                openAttachmentPreviewer('view', (curDocument._links as DynDBAttachmentLinks).self)
            },
            enabled: true
        })

        // delete
        buttonBar.AddButton({
            id: 'delete',
            type: 'toolbar',
            position: 'left',
            displayText: 'Delete',
            icon: 'fasUser',
            color: 'primary',
            action: () => {
                openAttachmentDeleter('view', (curDocument._links as DynDBAttachmentLinks).self)
            },
            enabled: true
        })

    }

    async function UpdateButtons(curDocument: JSONSingleDoc | undefined) {
        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        const curDocumentType = api.document.DetectDocType(curDocument)
        if (curDocumentType === 'attachment') {
            buttonBar.EnableButton('preview')
            buttonBar.EnableButton('delete')
            return
        }

        buttonBar.DisableButton('preview')
        buttonBar.DisableButton('delete')
    }

    onMounted(() => {
        SetupButtons(curDocument.value)
        UpdateButtons(curDocument.value)
    })
    watch(curDocument, (newValue) => {
        SetupButtons(newValue)
        UpdateButtons(newValue)
    })
}
