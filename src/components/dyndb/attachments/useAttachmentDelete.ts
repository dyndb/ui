// components
import { RegisterNativeComponent } from "@/api/globalComponents"
import dyndbAttachmentDelete from '@/components/dyndb/attachments/dyndb-preview-delete.vue'


export function useAttachmentDelete() {
    RegisterNativeComponent('dyndb-attachment-delete', dyndbAttachmentDelete)
}
