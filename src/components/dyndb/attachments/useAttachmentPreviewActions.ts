import { useButtonbarStore } from "@/stores/buttonbar";
import { onMounted, watch, type Ref } from "vue";


export function useAttachmentPreviewActions(
    stackName: string,
    prevEnabled: Ref<boolean>, nextEnabled: Ref<boolean>,
    onAction: (action: 'prev' | 'next') => void,
) {


    function SetupButtons() {

        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        // Next
        buttonBar.AddButton({
            id: 'next',
            type: 'toolbar',
            position: 'left',
            displayText: 'Next',
            icon: 'fasForward',
            color: 'primary',
            action: () => {
                onAction('next')
            },
            enabled: nextEnabled.value
        })

        // Prev
        buttonBar.AddButton({
            id: 'prev',
            type: 'toolbar',
            position: 'left',
            displayText: 'Prev',
            icon: 'fasBackward',
            color: 'primary',
            action: () => {
                onAction('prev')
            },
            enabled: prevEnabled.value
        })
    }

    onMounted(() => {
        SetupButtons()
    })
    watch(prevEnabled, (newValue) => {
        SetupButtons()
    })
    watch(nextEnabled, (newValue) => {
        SetupButtons()
    })

}
