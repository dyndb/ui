import { useLoginStore } from '@/stores/login'
import { fasCopy } from '@quasar/extras/fontawesome-v6'

import api from '@/api'
import { type DynWindow } from '@/api/windows'

// components
import { RegisterNativeComponent } from '@/api/globalComponents'
import dynDBAttachmentOverview from '@/components/dyndb/attachments/overview.vue'
RegisterNativeComponent('dyndb-attachments-overview', dynDBAttachmentOverview)

export function useAttachmentsOverview() {
  const loginstore = useLoginStore()

  loginstore.$subscribe(async (mutation, state) => {
    if (state.loggedin !== undefined) {
      if (state.loggedin === true) {
        api.windows.SetWindow('static', {
          ID: 'attachments',
          icon: fasCopy,
          displayName: 'Attachments',
          component: 'dyndb-attachments-overview',
          componentParameter: {},
        } as DynWindow)

      }
    }
  })

}
