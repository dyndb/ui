import api from '@/api'
import type { DynWindow } from '@/api/windows'
import { useLoginStore } from '@/stores/login'
import { fasShoppingCart } from '@quasar/extras/fontawesome-v5'

// components
import { RegisterNativeComponent } from '@/api/globalComponents'
import dyndbCollectorOverview from '@/components/dyndb/collector/collector.vue'
RegisterNativeComponent('dyndb-collector-overview', dyndbCollectorOverview)


export function useCollectorOverview() {
    const loginstore = useLoginStore()

    loginstore.$subscribe((mutation, state) => {
        if (state.loggedin !== undefined) {
            if (state.loggedin === true) {

                api.windows.SetWindow('static', {
                    ID: 'collector-overview',
                    icon: fasShoppingCart,
                    displayName: 'Collector',
                    component: 'dyndb-collector-overview',
                } as DynWindow)


            } else {
                api.windows.Close('search')
            }
        }
    })
}
