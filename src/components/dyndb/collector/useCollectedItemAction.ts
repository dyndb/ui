import type { JSONSingleDoc } from "@/api/document";
import { useButtonbarStore } from "@/stores/buttonbar";
import { onMounted, watch, type Ref } from "vue";


export function useCollectedItemAction(
    stackName: string,
    curDocumentIndex: Ref<number | undefined>,
    onAction: (action: 'addToLinked' | 'uncollect', index: number | undefined) => void
) {


    const buttonbarStore = useButtonbarStore()
    let buttonBar = buttonbarStore.Stack(stackName)


    // add to linked
    buttonBar.AddButton({
        id: 'addToLinked',
        type: 'toolbar',
        position: 'left',
        displayText: 'For Link',
        icon: 'fasHandPointer',
        color: 'primary',
        style: {
            'min-width': '100px',
            'min-height': '100%'
        },
        action: async () => {
            onAction('addToLinked', curDocumentIndex.value)
        },
        enabled: true
    })

    // uncollect
    buttonBar.AddButton({
        id: 'uncollect',
        type: 'toolbar',
        position: 'left',
        displayText: 'Uncollect',
        icon: 'fasMinus',
        color: 'primary',
        style: {
            'min-width': '100px',
            'min-height': '100%'
        },
        action: async () => {
            onAction('uncollect', curDocumentIndex.value)
        },
        enabled: true
    })

    buttonbarStore.AddStack(buttonBar)


    function UpdateButtons(curDocumentIndex: number | undefined) {
        let buttonBar = buttonbarStore.CurrentStack()
        if (buttonBar === undefined) { return }

        if (curDocumentIndex !== undefined) {
            buttonBar.EnableButton('addToLinked')
            buttonBar.EnableButton('uncollect')
        } else {
            buttonBar.DisableButton('addToLinked')
            buttonBar.DisableButton('uncollect')
        }
    }

    onMounted(() => { UpdateButtons(curDocumentIndex.value) })
    watch(curDocumentIndex, (newValue) => { UpdateButtons(newValue) })

}
