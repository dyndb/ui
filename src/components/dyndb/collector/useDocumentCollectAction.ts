import api from "@/api";
import type { JSONSingleDoc } from "@/api/document";
import { IsError } from "@/api/error";
import { useButtonbarStore } from "@/stores/buttonbar";
import { useNotificationStore } from "@/stores/notification";
import { onMounted, watch, type Ref } from "vue";

// components

export function useDocumentCollectAction(
    stackName: string,
    curDocument: Ref<JSONSingleDoc | undefined>
) {


    const notificationStore = useNotificationStore()

    function SetupButtons(curDocument: JSONSingleDoc | undefined) {
        if (curDocument === undefined) { return }

        const curDocumentType = api.document.DetectDocType(curDocument)

        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        // collect
        if (curDocumentType === 'document' || curDocumentType === 'attachment') {
            buttonBar.AddButton({
                id: 'collect',
                type: 'toolbar',
                position: 'left',
                displayText: 'Collect',
                icon: 'fasCartPlus',
                color: 'primary',
                action: async () => {
                    try {
                        let loadedDocument: JSONSingleDoc = {} as JSONSingleDoc
                        if (curDocumentType === 'document') {
                            loadedDocument = await api.document.Fetch(curDocument?._links?.self)
                        }
                        if (curDocumentType === 'attachment') {
                            loadedDocument = await api.attachments.Fetch(curDocument?._links?.self)
                        }

                        // ensure icon
                        if (loadedDocument.faicon === undefined) {
                            loadedDocument.faicon = 'fasSheetPlastic'
                        }

                        api.collection.CollectDocument(loadedDocument)
                        api.notifications.Sucess('collector.status.collected')
                    }
                    catch (e) {
                        if (typeof e === 'string') {
                            notificationStore.ShowError({ name: 'Error', message: e })
                        }
                        if (IsError(e)) {
                            notificationStore.ShowError(e)
                        }
                    }


                },
                enabled: false
            })
        }
    }

    function UpdateButtons(curDocument: JSONSingleDoc | undefined) {
        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        const curDocumentType = api.document.DetectDocType(curDocument)

        if (curDocumentType === 'document' || curDocumentType === 'attachment') {
            buttonBar?.EnableButton('collect')
        } else {
            buttonBar?.DisableButton('collect')
        }
    }

    onMounted(() => {
        SetupButtons(curDocument.value)
        UpdateButtons(curDocument.value)
    })
    watch(curDocument, (newValue) => {
        SetupButtons(newValue)
        UpdateButtons(newValue)
    })

}
