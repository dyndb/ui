import { onMounted, onUnmounted, ref, watch } from "vue";

const autoRefreshTimer: Record<string, number> = {}

export type useAutoRefreshOpts = {
    id: string
    seconds: number,
    state: Record<string, boolean>,
    refresh: () => void,
}

export function useAutoRefresh(opts: useAutoRefreshOpts) {

    // load it
    let enabled: boolean | undefined = opts.state[opts.id]
    if (enabled === undefined) {
        enabled = true
    }

    // reactive
    const autoRefreshEnabled = ref(enabled)

    function HandleSwitch(value: boolean) {

        // stop active interval
        let timerID = autoRefreshTimer[opts.id]
        if (timerID !== undefined) {
            console.debug("[TIMER] STOP", timerID)
            clearInterval(timerID)
            delete (autoRefreshTimer[opts.id])
        }

        // start
        if (value) {
            autoRefreshTimer[opts.id] = setInterval(opts.refresh, 1000 * opts.seconds) as unknown as number
            console.debug("[TIMER] START", autoRefreshTimer[opts.id])
        }

        opts.state[opts.id] = value
    }

    watch(autoRefreshEnabled, (value: boolean) => { HandleSwitch(value) })
    onMounted(() => { HandleSwitch(autoRefreshEnabled.value) })
    onUnmounted(() => { HandleSwitch(false) })

    return { autoRefreshEnabled }
}