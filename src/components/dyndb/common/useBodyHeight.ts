import { onMounted, ref } from 'vue'

function GetBodyDimension() {
  const element = document.body
  const styles = window.getComputedStyle(element)
  return {
    w: Number.parseFloat(styles.width),
    h: Number.parseFloat(styles.height)
  }
}

function GetElementDimension(id: string) {
  const element = document.getElementById(id)
  if (element !== null) {
    const styles = window.getComputedStyle(element)
    return {
      w: Number.parseFloat(styles.width),
      h: Number.parseFloat(styles.height)
    }
  }

  return {
    w: 100,
    h: 100
  }
}

export function useBodyDimension() {
  const bodyWidth = ref(100)
  const bodyHeight = ref(100)

  onMounted(() => {
    const values = GetBodyDimension()
    bodyWidth.value = values.w
    bodyHeight.value = values.h
  })

  return { bodyWidth, bodyHeight }
}

export function useElementDimension(id: string) {
  const elementWidth = ref(100)
  const elementHeight = ref(100)
  onMounted(() => {
    const values = GetElementDimension(id)
    elementWidth.value = values.w
    elementHeight.value = values.h
  })

  return { elementWidth, elementHeight }
}
