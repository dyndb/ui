import api from '@/api'
import { type DynWindow } from '@/api/windows'
import { useLoginStore } from '@/stores/login'
import { fasCodeMerge } from '@quasar/extras/fontawesome-v6'


// components
import { RegisterNativeComponent } from '@/api/globalComponents'
import dyndbConfig from '@/components/dyndb/config/config.vue'
RegisterNativeComponent('dyndb-config', dyndbConfig)


export function useConfigWindow() {
  const loginstore = useLoginStore()


  loginstore.$subscribe(async (mutation, state) => {
    if (state.loggedin !== undefined) {
      if (state.loggedin === true) {


        api.windows.SetWindow('static', {
          ID: 'config',
          icon: fasCodeMerge,
          displayName: 'Config',
          component: 'dyndb-config',
        } as DynWindow)

      }
    }
  })
}
