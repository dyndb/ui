import api from "@/api"
import type { DynDBAttachmentLinks } from "@/api/attachment"
import type { DocumentAction } from "@/api/document"
import { errors } from "@/api/error"
import type { DynWindow } from "@/api/windows"
import { useNotificationStore } from "@/stores/notification"
import { fasFilePdf } from "@quasar/extras/fontawesome-v6"


// components
import { RegisterNativeComponent } from '@/api/globalComponents'
import dyndbDocumentAscards from '@/components/dyndb/document/dyndb-document-ascards.vue'
RegisterNativeComponent('dyndb-document-ascards', dyndbDocumentAscards)


export async function openDocumentEditor(mode: DocumentAction, selfURL: string | undefined) {
    if (selfURL === undefined) { return }


    const notificationStore = useNotificationStore()

    const loadedDocument = await api.document.Fetch(selfURL)
    const loadedDocumentType = api.document.DetectDocType(loadedDocument)

    if (loadedDocument === undefined) {
        notificationStore.ShowError(errors.PreconditionFailed('loadedDocument is undefined'))
        return
    }

    if (loadedDocumentType === 'document' || loadedDocumentType === 'field' || loadedDocumentType === 'template') {
        api.windows.SetWindow(mode === 'preview' ? 'overlayed' : 'dynamic', {
            ID: loadedDocument._links?.self + "_" + mode,
            icon: loadedDocument.faicon,
            displayName: loadedDocument.displayText,
            closable: true,

            component: 'dyndb-document-ascards',
            componentParameter: {
                document: loadedDocument,
                mode: mode
            },
            index: 1
        } as DynWindow)

        // if (action !== 'view') {
        api.windows.SetWindowActive(selfURL + "_" + mode)
        // }
    }

    if (loadedDocumentType === 'attachment') {
        api.windows.SetWindow(mode === 'preview' ? 'overlayed' : 'dynamic', {
            ID: (loadedDocument._links as DynDBAttachmentLinks).preview,
            icon: fasFilePdf,
            displayName: loadedDocument.displayText,
            closable: true,

            component: 'dyndb-attachment-preview',
            componentParameter: {
                url: '/api/v1' + (loadedDocument._links as DynDBAttachmentLinks).preview,
                //url: 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf',
            },
        } as DynWindow)

        api.windows.SetWindowActive((loadedDocument._links as DynDBAttachmentLinks).preview)

    }

}
