import api from "@/api";
import type { JSONSingleDoc } from "@/api/document";
import { useButtonbarStore } from "@/stores/buttonbar";
import { onMounted, watch, type Ref } from "vue";
import { openDocumentEditor } from "./openDocumentEditor";
import { RegisterNativeComponent } from "@/api/globalComponents";
import dyndbExplorer from '@/components/dyndb/explorer/explorer.vue'
// components

export function useDocumentEditAction(
    stackName: string,
    curDocument: Ref<JSONSingleDoc | undefined>
) {

    RegisterNativeComponent('dyndb-explorer', dyndbExplorer)

    function SetupButtons(curDocument: JSONSingleDoc | undefined) {
        if (curDocument === undefined) { return }

        const curDocumentType = api.document.DetectDocType(curDocument)

        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        // edit
        if (curDocumentType === 'document' || curDocumentType === 'field' || curDocumentType === 'template') {
            buttonBar.AddButton({
                id: 'edit',
                type: 'toolbar',
                position: 'left',
                displayText: 'Edit',
                icon: 'fasPencil',
                color: 'primary',
                action: async () => {
                    openDocumentEditor('edit', curDocument?._links?.self)
                },
                enabled: false
            })
        }
    }

    function UpdateButtons(curDocument: JSONSingleDoc | undefined) {
        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        const curDocumentType = api.document.DetectDocType(curDocument)

        if (curDocumentType === 'document' || curDocumentType === 'field' || curDocumentType === 'template') {
            buttonBar?.EnableButton('edit')
        } else {
            buttonBar?.DisableButton('edit')
        }
    }

    onMounted(() => {
        SetupButtons(curDocument.value)
        UpdateButtons(curDocument.value)
    })
    watch(curDocument, (newValue) => {
        SetupButtons(newValue)
        UpdateButtons(newValue)
    })

}
