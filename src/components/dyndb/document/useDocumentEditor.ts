import { RegisterNativeComponent } from "@/api/globalComponents";

import dyndbDocumentAsCards from '@/components/dyndb/document/dyndb-document-ascards.vue'

export function useDocumentEditor() {
    RegisterNativeComponent('dyndb-document-ascards', dyndbDocumentAsCards)
}