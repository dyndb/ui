import type { JSONSingleDoc } from "@/api/document";
import { useButtonbarStore } from "@/stores/buttonbar";
import { onMounted, watch, type Ref } from "vue";





export function useDocumentEditorActions(
    stackName: string,
    curDocument: Ref<JSONSingleDoc | undefined>,
    onAction: (action: 'saved' | 'deleted', item: JSONSingleDoc | undefined) => void
) {

    const buttonbarStore = useButtonbarStore()
    let buttonBar = buttonbarStore.Stack(stackName)

    function SetupButtons(curDocument: JSONSingleDoc | undefined) {
        if (curDocument === undefined) { return }

        // Save
        buttonBar.AddButton({
            id: 'save',
            type: 'actionbar',
            position: 'right',
            displayText: 'Save',
            icon: 'fasCheck',
            color: 'primary',
            style: {
                "min-width": "150px"
            },
            action: async () => {
                onAction('saved', curDocument)
            },
            enabled() {
                return curDocument?._links?.save !== ""
            },
        })

        // Delete
        buttonBar.AddButton({
            id: 'delete',
            type: 'actionbar',
            position: 'right',
            displayText: 'Delete',
            icon: 'fasTrashCan',
            color: 'warning',
            style: {
                "min-width": "150px"
            },
            needConfirm: true,
            action: () => {
                onAction('deleted', curDocument)
            },
            enabled() {
                return curDocument?._links?.delete !== ""
            },
        })
    }


    onMounted(() => {
        SetupButtons(curDocument.value)
        //    UpdateButtons(curDocument.value)
    })
    watch(curDocument, (newValue) => {
        SetupButtons(newValue)
        //  UpdateButtons(newValue)
    })

}
