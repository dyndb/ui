import api from "@/api";
import type { DocumentAction, JSONSingleDoc } from "@/api/document";
import type { Template } from "@/api/templates";
import type { DynWindow } from "@/api/windows";
import { ButtonbarStack, useButtonbarStore } from "@/stores/buttonbar";
import { useDocumentsStore } from "@/stores/documents";
import { useWindowStore } from "@/stores/windows";
import { onMounted, onUnmounted, watch, type Ref } from "vue";


export function useDocumentNavigation(
    stackName: string,
    curTemplate: Ref<string | undefined>
) {

    const windowStore = useWindowStore()
    const documentsStore = useDocumentsStore()
    const buttonbarStore = useButtonbarStore()

    function SetupButtons() {
        let buttonBar = buttonbarStore.Stack(stackName)

        // Next
        buttonBar.AddButton({
            id: 'list_next',
            type: 'toolbar',
            position: 'left',
            displayText: 'Next',
            icon: 'fasForward',
            color: 'primary',
            action: () => {
                api.documents.ListNext()
            },
            enabled: () => {
                return documentsStore.NextPossible
            }
        })

        // Prev
        buttonBar.AddButton({
            id: 'list_prev',
            type: 'toolbar',
            position: 'left',
            displayText: 'Prev',
            icon: 'fasBackward',
            color: 'primary',
            action: () => {
                api.documents.ListPrev()
            },
            enabled: () => {
                return documentsStore.PrevPossible
            }
        })

        // refresh
        buttonBar.AddButton({
            id: 'list_refresh',
            type: 'toolbar',
            position: 'left',
            displayText: 'Refresh',
            icon: 'fasArrowsSpin',
            color: 'primary',
            action: () => {
                api.documents.Refresh()
            },
            enabled: () => {
                return documentsStore.RefreshPossible
            }

        })

        // new
        buttonBar.AddButton({
            id: 'new',
            type: 'toolbar',
            position: 'left',
            displayText: 'New',
            icon: 'fasSquarePlus',
            color: 'primary',
            action: async () => {
                let createdDocument = await api.document.Create(curTemplate.value)

                const newWindowID = await api.windows.SetWindow('dynamic', {
                    ID: createdDocument._links?.self,
                    icon: 'fasUser',
                    displayName: "New",
                    closable: true,

                    component: 'dyndb-document-ascards',
                    componentParameter: {
                        document: createdDocument,
                        mode: "edit" as DocumentAction
                    },
                    index: 1
                } as DynWindow)

                api.windows.SetWindowActive(newWindowID)
            },
            enabled: false
        })


    }

    function UpdateButtons(doc: string | undefined) {
        let buttonBar = buttonbarStore.Stack(stackName)


        if (doc !== undefined) {
            buttonBar.EnableButton('new')
        } else {
            buttonBar.DisableButton('new')
        }
    }

    onMounted(() => {
        SetupButtons()
        UpdateButtons(curTemplate.value)
    })
    watch(curTemplate, (newValue) => {
        UpdateButtons(newValue)
    })

}
