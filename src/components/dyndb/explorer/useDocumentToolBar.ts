import { onMounted, onUnmounted, watch, type Ref } from "vue"
import { ButtonbarStack, useButtonbarStore } from "@/stores/buttonbar"

export function useDocumentToolBar(id: string) {

    const buttonbarStore = useButtonbarStore()

    onMounted(() => {
        buttonbarStore.Stack(id)
    })
    onUnmounted(async () => {
        buttonbarStore.Close(id)
    })

}