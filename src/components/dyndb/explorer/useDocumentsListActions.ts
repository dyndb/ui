import api from "@/api";
import type { JSONSingleDoc } from "@/api/document";
import type { Template } from "@/api/templates";
import type { DynWindow } from "@/api/windows";
import { useButtonbarStore } from "@/stores/buttonbar";
import { useNotificationStore } from "@/stores/notification";
import { onMounted, watch, type Ref } from "vue";
import { RegisterNativeComponent } from "@/api/globalComponents";
import dyndbExplorer from '@/components/dyndb/explorer/explorer.vue'

// components

export function useDocumentsListActions(
    stackName: string,
    curDocument: Ref<JSONSingleDoc | undefined>
) {

    RegisterNativeComponent('dyndb-explorer', dyndbExplorer)

    const buttonbarStore = useButtonbarStore()
    const notificationStore = useNotificationStore()

    function SetupButtons(curDocument: JSONSingleDoc | undefined) {
        if (curDocument === undefined) { return }

        const curDocumentType = api.document.DetectDocType(curDocument)

        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        // list
        if (curDocumentType === 'template') {
            buttonBar.AddButton({
                id: 'list',
                type: 'toolbar',
                position: 'left',
                displayText: 'List',
                icon: 'fasList',
                color: 'primary',
                action: async () => {
                    try {
                        const loadedDocument = await api.document.Fetch(curDocument?._links?.self)
                        const curDocumentType = api.document.DetectDocType(loadedDocument)


                        if (curDocumentType === 'template') {

                            let templateName = (loadedDocument as Template).tplName

                            let newWindowID = await api.windows.SetWindow('dynamic', {
                                ID: 'list_' + templateName,
                                icon: loadedDocument.faicon,
                                displayName: (loadedDocument as Template).displayText,
                                closable: true,

                                component: 'dyndb-explorer',
                                componentParameter: {
                                    templatename: templateName,
                                },
                            } as DynWindow)

                            api.windows.SetWindowActive(newWindowID)

                        }
                    }
                    catch (e) {
                        if (typeof e === 'string') {
                            notificationStore.ShowError({ name: 'Error', message: e })
                        }
                        if (e instanceof Error) {
                            notificationStore.ShowError(e)
                        }
                    }


                },
                enabled: false
            })
        }

    }

    function UpdateButtons(curDocument: JSONSingleDoc | undefined) {
        const buttonbarStore = useButtonbarStore()
        let buttonBar = buttonbarStore.Stack(stackName)

        const curDocumentType = api.document.DetectDocType(curDocument)

        if (curDocumentType === 'template') {
            buttonBar?.EnableButton('list')
        } else {
            buttonBar?.DisableButton('list')
        }
    }

    onMounted(() => {
        SetupButtons(curDocument.value)
        UpdateButtons(curDocument.value)
    })
    watch(curDocument, (newValue) => {
        SetupButtons(newValue)
        UpdateButtons(newValue)
    })

}
