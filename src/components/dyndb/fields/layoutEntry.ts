import { type Field } from '@/api/fields'

export interface LayoutEntry {
  field: Field
  x: number
  y: number
  colspan: number
  rowspan: number

  position: number
}
