import api from '@/api'
import { type TemplateLayoutField } from '@/api/templates'
import { PatchLayoutArrays } from './patchLayoutArrays'
import type { JSONSingleDoc } from '@/api/document'
import type { LayoutEntry } from './layoutEntry'
import { IsError } from '@/api/error'

/**
 *
 * @param layoutDocEntrys Your current layout entrys
 * @param templateName the template name from where the layout will be loaded
 * @returns
 */
export async function PatchLayoutFromTemplate(
  layoutDocEntrys: LayoutEntry[],
  templateName: string | undefined
) {
  // temp
  let tmpLayoutDocEntrys: LayoutEntry[] = layoutDocEntrys

  // load fields from template
  if (templateName !== undefined) {
    const tmpLayoutFields = await api.templates.Layout(templateName)
    if (!IsError(tmpLayoutFields)) {
      tmpLayoutDocEntrys = await PatchLayoutArrays(tmpLayoutDocEntrys, tmpLayoutFields)
    } else {
      console.error(tmpLayoutFields)
    }
  }

  return tmpLayoutDocEntrys
}

export async function PatchLayoutFromDocFields(
  layoutDocEntrys: LayoutEntry[],
  doc: JSONSingleDoc | undefined
) {
  // temp
  let tmpLayoutDocEntrys: LayoutEntry[] = layoutDocEntrys

  // load fields from doc
  if (doc !== undefined) {
    const tmpLayoutDocFields = api.document.LayoutFromDocFields(doc)
    console.debug('docLayout: layout loaded from fields of current document', tmpLayoutDocFields)

    tmpLayoutDocEntrys = await PatchLayoutArrays(tmpLayoutDocEntrys, tmpLayoutDocFields)
  }

  return tmpLayoutDocEntrys
}

export async function PatchLayoutFromDocLayout(
  layoutDocEntrys: LayoutEntry[],
  layoutFromDoc: TemplateLayoutField[] | undefined
) {
  // temp
  let tmpLayoutDocEntrys: LayoutEntry[] = layoutDocEntrys

  tmpLayoutDocEntrys = await PatchLayoutArrays(tmpLayoutDocEntrys, layoutFromDoc)

  return tmpLayoutDocEntrys
}

export async function ToTemplateLayoutFields(
  entrys: LayoutEntry[]
): Promise<TemplateLayoutField[]> {
  const templateLayoutFields: TemplateLayoutField[] = []
  for (const entry of entrys) {
    templateLayoutFields.push({
      fieldName: entry.field.fieldName,
      x: entry.x,
      y: entry.y,
      colspan: entry.colspan,
      rowspan: entry.rowspan
    } as TemplateLayoutField)
  }
  return templateLayoutFields
}
