import api from '@/api'
import { type LayoutEntry } from './layoutEntry'
import { type TemplateLayoutField } from '@/api/templates'

export async function PatchLayoutArrays(
  layouts: LayoutEntry[] | undefined,
  layoutsToPatch: TemplateLayoutField[] | undefined
): Promise<LayoutEntry[]> {
  if (layouts === undefined) return []
  if (layoutsToPatch === undefined) return layouts

  // for simple access
  const layoutsToPatchMap: Record<string, TemplateLayoutField> = {}
  for (const layoutToPatch of layoutsToPatch) {
    layoutsToPatchMap[layoutToPatch.fieldName] = layoutToPatch
  }

  // patch existing
  layouts = layouts.filter((layout: LayoutEntry, index: number) => {

    if (layout.field === undefined) {
      return false
    }

    const layoutForPatch = layoutsToPatchMap[layout.field.fieldName]
    if (layoutForPatch === undefined) {
      return true
    }

    if (layoutForPatch.x !== undefined) {
      layout.x = layoutForPatch.x
    }
    if (layoutForPatch.rowspan !== undefined) {
      layout.rowspan = layoutForPatch.rowspan
    }
    if (layoutForPatch.y !== undefined) {
      layout.y = layoutForPatch.y
    }
    if (layoutForPatch.colspan !== undefined) {
      layout.colspan = layoutForPatch.colspan
    }

    delete layoutsToPatchMap[layout.field.fieldName]
    return true
  })



  // load fields from backend if needed
  const fieldNamesArray = Array.from(Object.keys(layoutsToPatchMap))
  await api.fields.FieldsByName(fieldNamesArray)

  // add not patched fields
  for (const layoutToPatch of Object.values(layoutsToPatchMap)) {
    // get the field
    // @todo use an "undefined" field
    const field = await api.fields.Field(layoutToPatch.fieldName)
    if (field === undefined) {
      console.warn(`Field '${layoutToPatch.fieldName}' not found`)
      continue
    }

    // default values
    if (layoutToPatch.x === undefined) {
      layoutToPatch.x = 0
    }
    if (layoutToPatch.colspan === undefined) {
      layoutToPatch.colspan = 6
    }
    if (layoutToPatch.y === undefined) {
      layoutToPatch.y = 0
    }
    if (layoutToPatch.rowspan === undefined) {
      layoutToPatch.rowspan = 1
    }

    layouts.push({
      field: field,
      x: layoutToPatch.x,
      y: layoutToPatch.y,
      colspan: layoutToPatch.colspan,
      rowspan: layoutToPatch.rowspan
    } as LayoutEntry)
  }


  return layouts
}
