import { RegisterNativeComponent } from '@/api/globalComponents'

export interface FieldProp {
  id: string
  label: string
  hint: string
  validationFunction?: Function
  invalidMessage?: string
}

import dyndbFieldString from './dyndb-field-string.vue'
import dyndbFieldFloat from './dyndb-field-float.vue'
import dyndbFieldDatatype from './dyndb-field-datatype.vue'
import dyndbFieldFaicon from './dyndb-field-faicon.vue'
import dyndbFieldDate from './dyndb-field-date.vue'
import dyndbFieldSingleedit from './dyndb-field-singledit.vue'

export function useFieldComponents() {
  RegisterNativeComponent('dyndb-field-string', dyndbFieldString)
  RegisterNativeComponent('dyndb-field-float', dyndbFieldFloat)
  RegisterNativeComponent('dyndb-field-datatype', dyndbFieldDatatype)
  RegisterNativeComponent('dyndb-field-faicon', dyndbFieldFaicon)
  RegisterNativeComponent('dyndb-field-date', dyndbFieldDate)
  RegisterNativeComponent('dyndb-field-singledit', dyndbFieldSingleedit)
}
