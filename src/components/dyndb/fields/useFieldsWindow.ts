import api from '@/api'
import type { Field } from '@/api/fields'
import type { DynWindow } from '@/api/windows'
import { type DocumentStore, useDocumentStore } from '@/stores/document'
import { useLoginStore } from '@/stores/login'
import { fasList } from '@quasar/extras/fontawesome-v6'
import type { SubscriptionCallbackMutation } from 'pinia'

import { IsError } from '@/api/error'
import dyndbExplorer from '@/components/dyndb/explorer/explorer.vue'
import { RegisterNativeComponent } from '@/api/globalComponents'

/**
 * Will add the fields window after login was successful or remove it if not
 */
export function useFieldsWindow() {


  const loginstore = useLoginStore()

  loginstore.$subscribe(async (mutation, state) => {
    if (state.loggedin !== undefined) {
      if (state.loggedin === true) {

        RegisterNativeComponent('dyndb-explorer', dyndbExplorer)


        // get all fields
        // DISABLED: Because fields will be fetched/cached on demand
        // api.fields.FetchAll()

        api.windows.SetWindow('static', {
          ID: 'fields',
          icon: fasList,
          displayName: 'Fields',
          component: 'dyndb-explorer',
          componentParameter: {
            templatename: 'dyndb_field',
          },
        } as DynWindow)
      }
    }
  })

  // @TODO: In future, this will be send over sse from the backend !
  const documentStore = useDocumentStore()
  documentStore.$subscribe(
    (mutation: SubscriptionCallbackMutation<DocumentStore>, state: DocumentStore) => {
      if (state.events === undefined) { return }

      let curDocument = state.events.value
      if (IsError(curDocument)) { return }
      let curDocumentType = api.document.DetectDocType(curDocument)

      if (curDocumentType !== 'field') { return }

      switch (state.events.event) {
        case 'saved':
          api.fields.Append(state.events.value as Field)
          break
        case 'deleted':
          api.fields.Remove(state.events.value as Field)
          break
      }
    }
  )
}
