import { fasLayerGroup } from '@quasar/extras/fontawesome-v6'
import { onActivated, onBeforeMount, onMounted } from 'vue'

var fontawesome: Record<string, string>

export function useFontAwesome() {


    onMounted(async () => {
        if (fontawesome === undefined) {
            fontawesome = await (import('@quasar/extras/fontawesome-v6')) as unknown as Record<string, string>
        }
    })




    function GetIconFromObjectField(obj: any, fieldName: string | undefined) {
        let iconName = ''

        if (fieldName !== undefined) {
            iconName = obj[fieldName]
        }

        if (iconName === undefined) {
            iconName = 'fasLayerGroup'
        }

        return GetIcon(iconName)
    }

    // load icon from name
    function GetIcon(iconName: string | undefined) {
        if (fontawesome === undefined) return fasLayerGroup
        if (iconName === undefined) {
            iconName = 'fasLayerGroup'
        }

        let iconNameFromObject = fontawesome[iconName]
        if (iconNameFromObject === undefined) {
            iconNameFromObject = fasLayerGroup
        }

        return iconNameFromObject
    }

    //const { x, y } = useMouse()

    return { GetIconFromObjectField, GetIcon }
}