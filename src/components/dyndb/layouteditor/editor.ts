import { fasDiagramProject } from '@quasar/extras/fontawesome-v6'
import { errors } from '@/api/error'

import api from '@/api'
import { type DynWindow } from '@/api/windows'
import { trimAny } from '@/api/Tools'

export interface EditorCommonProps {
  id: string
  label: string
  hint: string
  validationFunction?: Function
  invalidMessage?: string
}

export async function AddLayoutWindow(
  displayName: string,
  selfURL: string | undefined
): Promise<string | Error> {
  if (selfURL === undefined) {
    return errors.PreconditionFailed('selfURL is undefined')
  }

  // convert self-url to ui-url
  const selfURLParts = trimAny(selfURL, '/').split('/')
  const docID = selfURLParts[1]

  const windowID = await api.windows.SetWindow('dynamic', {
    ID: `/layout/${docID}`,
    icon: fasDiagramProject,
    displayName: displayName,
    uri: `/layout/${docID}`,
    closable: true
  } as DynWindow)

  return windowID
}
