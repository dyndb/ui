import { onMounted } from 'vue'
import { useRouter } from 'vue-router'

import api from '@/api'
import { type DynWindow } from '@/api/windows'
import { useLoginStore } from '@/stores/login'
import { fasUser } from '@quasar/extras/fontawesome-v6'

// components
import dyndbLoginWindow from './dyndb-login.vue'
import { RegisterNativeComponent } from '@/api/globalComponents'
RegisterNativeComponent('dyndb-login', dyndbLoginWindow)

export function useLoginWindow() {
  const router = useRouter()
  const loginstore = useLoginStore()

  loginstore.$subscribe((mutation, state) => {
    if (state.loggedin !== undefined) {
      if (state.loggedin === false) {
        api.windows.SetWindow('overlayed', {
          ID: '/login',
          icon: fasUser,
          displayName: 'Login',

          component: 'dyndb-login'
        } as DynWindow)
      } else {
        api.windows.Close('/login')
      }
    }
  })

  onMounted(async () => {
    // fetch user info
    await api.auth.FetchUserInfo()
  })
}
