import api from '@/api'
import { useLoginStore } from '@/stores/login'

export function useMessaging() {
  /*
  const messagingStore = useMessagingStore();
  const messagingHistoryStore = useMessagingHistoryStore();
  messagingStore.$subscribe(
    (
      mutation: SubscriptionCallbackMutation<MessagingStore>,
      state: MessagingStore
    ) => {
      messagingHistoryStore.Add(state.message);
    }
  );
  */

  const loginstore = useLoginStore()

  loginstore.$subscribe((mutation, state) => {
    if (state.loggedin !== undefined) {
      if (state.loggedin === true) {
        api.sse.Connect()
      }
    }
  })
}
