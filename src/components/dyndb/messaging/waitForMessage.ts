import { type MessagingStore, useMessagingStore } from '@/stores/messaging'
import { type SubscriptionCallbackMutation } from 'pinia'
import { type Ref } from 'vue'

export function waitForMessage(
  topic: string,
  loading: Ref<boolean>,
  timeout: Ref<boolean>,
  callback: (topic: string | undefined, payload: object | string | undefined) => void
) {
  // timer that shows "loading"
  const loadingTimer = window.setTimeout(() => {
    loading.value = true
  }, 500)

  // subscribe
  const messagingStore = useMessagingStore()
  const unsubscribe = messagingStore.$subscribe(function (
    mutation: SubscriptionCallbackMutation<MessagingStore>,
    state: MessagingStore
  ) {
    if (state.message?.topic === topic) {
      window.clearTimeout(loadingTimer)
      loading.value = false
      callback(state.message?.topic, state.message?.payload)
      unsubscribe()
    }
  })

  // timeout
  const timeoutTimer = window.setTimeout(() => {
    timeout.value = true
    unsubscribe()
  }, 10000)
}
