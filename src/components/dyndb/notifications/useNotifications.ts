import { useNotificationStore, type NotificationStore } from '@/stores/notification'
import type { SubscriptionCallbackMutation } from 'pinia'
import { type QNotifyCreateOptions, useQuasar } from 'quasar'

export function useNotifications() {
  const $q = useQuasar()

  const notificationStore = useNotificationStore()
  notificationStore.$subscribe(
    (mutation: SubscriptionCallbackMutation<NotificationStore>, state: NotificationStore) => {
      if (state.message === undefined) return



      if (typeof state.message.detail === 'object') {
        if (state.message && state.message.detail && Array.isArray(state.message.detail)) {
          state.message.detail = JSON.stringify(state.message.detail[0])
        } else {
          state.message.detail = JSON.stringify(state.message.detail)
        }
      }

      $q.notify({
        progress: true,
        caption: state.message.detail,
        message: state.message.message,
        color: state.message.color,
        multiLine: true,
        position: 'top-right',
        timeout: 3000
      } as QNotifyCreateOptions)
    }
  )
}
