import { useLoginStore } from '@/stores/login'
import { fasUser } from '@quasar/extras/fontawesome-v6'

import api from '@/api'
import { RegisterNativeComponent } from '@/api/globalComponents'
import { type DynWindow } from '@/api/windows'

// components
import type { SearchWindowStateType } from '@/components/dyndb/search/search.vue'
import dyndbSearch from '@/components/dyndb/search/search.vue'
RegisterNativeComponent('dyndb-search', dyndbSearch)


export function useSearchWindow() {
  const loginstore = useLoginStore()

  loginstore.$subscribe((mutation, state) => {
    if (state.loggedin !== undefined) {
      if (state.loggedin === true) {


        api.windows.SetWindow('static', {
          ID: 'search',
          icon: fasUser,
          displayName: 'Search',
          component: 'dyndb-search',
          createInstance: true,
          state: {
            searchForDocuments: true,
            searchForAttachments: true,
            searchText: "",
            documents: []
          } as SearchWindowStateType,
        } as DynWindow)


      } else {
        api.windows.Close('search')
      }


    }
  })
}
