import api from '@/api'
import { IsError } from '@/api/error'
import type { Template } from '@/api/templates'
import { type DynWindow } from '@/api/windows'
import { RegisterNativeComponent } from '@/api/globalComponents'
import { useDocumentStore, type DocumentStore } from '@/stores/document'
import { useLoginStore } from '@/stores/login'
import { type SubscriptionCallbackMutation } from 'pinia'
import { fasCopy } from '@quasar/extras/fontawesome-v6'
import dyndbExplorer from '@/components/dyndb/explorer/explorer.vue'

export function useTemplatesWindow() {
  const loginstore = useLoginStore()

  loginstore.$subscribe(async (mutation, state) => {
    if (state.loggedin !== undefined) {
      if (state.loggedin === true) {

        RegisterNativeComponent('dyndb-explorer', dyndbExplorer)

        // get all templates
        await api.templates.FetchAll()

        api.windows.SetWindow('static', {
          ID: 'templates',
          icon: fasCopy,
          displayName: 'Templates',
          component: 'dyndb-explorer',
          componentParameter: {
            templatename: 'dyndb_template',
          },
        } as DynWindow)

      }
    }
  })

  // @TODO: In future, this will be send over sse from the backend !
  const documentStore = useDocumentStore()
  documentStore.$subscribe(
    (mutation: SubscriptionCallbackMutation<DocumentStore>, state: DocumentStore) => {
      if (state.events === undefined) { return }

      let curDocument = state.events.value
      if (IsError(curDocument)) { return }
      let curDocumentType = api.document.DetectDocType(curDocument)

      if (curDocumentType !== 'template') { return }

      switch (state.events.event) {
        case 'saved':
          api.templates.Append(state.events.value as Template)
          break
        case 'deleted':
          api.templates.Remove(state.events.value as Template)
          break
      }
    }
  )
}
