import { useConfigStore } from '@/stores/config'
import { onMounted } from 'vue'

export function useTheme() {
  const configStore = useConfigStore()

  function ImportTheme(theme: string) {
    const themeasset = import(`../../../assets//themes/${theme}/theme.css`)
    console.debug(themeasset)

    /*
    console.debug(new URL(`/assets/themes/${theme}/theme.css`, import.meta.url))

    const file = document.createElement('link')
    file.id = 'dyndb-theme'
    file.rel = 'stylesheet'
    file.href = new URL(`/assets/themes/${theme}/theme.css`, import.meta.url).pathname
    document.head.appendChild(file)
    */

    //document.getElementById('dyndb-theme').src = imgUrl
    /*
    const body = document.body
    body.className = `desktop no-touch body--light theme-${theme}`
*/
    /*
    const file = document.createElement('link')
    file.rel = 'stylesheet'
    file.href = '/assets/themes/dark/theme.css'
    document.head.appendChild(file)

    switch (theme) {
      case 'dark':
        import.meta.glob('../../../../assets/themes/dark/theme.css', {
          query: '?inline'
        })
        break

      default:
        import.meta.glob('../../../../assets/themes/default/theme.css', {
          query: '?inline'
        })
    }
    */
  }

  onMounted(() => {
    ImportTheme(configStore.$state.themeName)
  })

  configStore.$subscribe((mutation, state) => {
    ImportTheme(state.themeName)
  })
}
