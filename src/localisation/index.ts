import { type Plugin } from 'vue'
import { createI18n } from 'vue-i18n'

// import messages from '@intlify/unplugin-vue-i18n/messages'

const messages = {
  de: {
    login: {
      username: ''
    }
  }
}

// Create VueI18n instance with options
const i18n = createI18n({
  legacy: false,
  globalInjection: true,
  locale: 'de', // set locale
  fallbackLocale: 'de',
  availableLocales: ['de'],
  messages
})

export default i18n
