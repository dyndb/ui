import { createApp } from 'vue'
import quasar from '@/quasar'
// styles
import './css'

//import App from './App.vue'
import main from '@/components/dyndb/layout/dyndb-main.vue'
import pinia from './stores'
import router from './router'

const app = createApp(main)

app.use(pinia)

app.use(router)
app.use(quasar)

app.mount('#app')

export default app
