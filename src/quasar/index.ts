import { type App } from 'vue'
import { Notify, Quasar } from 'quasar'
import quasarIconSet from 'quasar/icon-set/svg-fontawesome-v6'

// Import icon libraries
import '@quasar/extras/fontawesome-v6/fontawesome-v6.css'

// Import Quasar css
import 'quasar/dist/quasar.css'

class UseQuasar {
  public install(app: App) {
    Quasar.install(app, {
      plugins: {
        Notify
      }, // import Quasar plugins and add here,
      iconSet: quasarIconSet
    } as any)
  }
}

const QuasarPlugin = new UseQuasar()
export default QuasarPlugin
