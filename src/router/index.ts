import { createRouter, createWebHistory, type RouteLocationNormalized } from 'vue-router'

import QuiDyndbLogin from '@/components/dyndb/login/dyndb-login.vue'
import QuiDyndbConfig from '@/components/dyndb/config/config.vue'
import QuiDyndbExplorer from '@/components/dyndb/explorer/explorer.vue'
import DynDBSearchWindow from '@/components/dyndb/search/search.vue'

const router = createRouter({
  //history: createWebHistory(import.meta.env.BASE_URL),
  history: createWebHistory(),
  routes: [
    { path: '/login', component: QuiDyndbLogin },
    { path: '/config', component: QuiDyndbConfig },
    { path: '/search', component: DynDBSearchWindow },

    {
      path: '/explorer/:templatename',
      component: QuiDyndbExplorer,
      props: true
    }
  ]
})

export default router
