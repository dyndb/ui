import { defineStore } from 'pinia'
import { v4 as uuidv4 } from 'uuid'

export interface ButtonbarButton {
  id: string
  displayText?: string
  color?: string
  icon?: string
  action?: () => void
  enabled?: (() => boolean) | boolean
  needConfirm?: boolean

  // or its an component
  componentName?: String
  componentProps?: any

  // position, size and so on
  type?: 'toolbar' | 'actionbar'
  position?: 'left' | 'right'
  columns?: number
  style?: {}
}

export class ButtonbarStack {
  private id: string
  private buttons: Array<ButtonbarButton>
  private payload: any

  constructor(id: string, payload: any) {
    if (id === '' || id === undefined) {
      id = uuidv4()
    }
    this.id = id
    this.buttons = new Array<ButtonbarButton>()
    this.payload = payload
  }

  public ID(): string {
    return this.id
  }
  public Buttons(): Array<ButtonbarButton> {
    return this.buttons
  }
  public Payload(): any {
    return this.payload
  }
  public PayloadSet(payload: any) {
    this.payload = payload
  }

  public AddButton(button: ButtonbarButton) {
    if (button.type === undefined) {
      button.type = 'actionbar'
    }

    // position
    if (button.position === undefined) {
      button.position = 'left'
    }

    // size and so on
    if (button.columns === undefined) {
      button.columns = 1
    }
    if (button.columns === 0) {
      button.columns = 1
    }

    // replace it
    for (const [curButtonIndex, curButton] of this.buttons.entries()) {
      if (curButton.id === button.id) {
        this.buttons[curButtonIndex] = button
        return
      }
    }

    // just add it
    switch (button.position) {
      case 'right':
        this.buttons.push(button)
        break
      default:
        this.buttons.unshift(button)
        break
    }
  }

  public GetButton(id: string): ButtonbarButton | undefined {
    for (const [curButtonIndex, curButton] of this.buttons.entries()) {
      if (curButton.id === id) {
        return curButton
      }
    }
  }

  public EnableButton(id: string) {
    for (const [curButtonIndex, curButton] of this.buttons.entries()) {
      if (curButton.id === id) {
        curButton.enabled = true
        return
      }
    }
  }

  public DisableButton(id: string) {
    for (const [curButtonIndex, curButton] of this.buttons.entries()) {
      if (curButton.id === id) {
        curButton.enabled = false
        return
      }
    }
  }

  public RemoveButton(id: string) {
    for (const [curButtonIndex, curButton] of this.buttons.entries()) {
      if (curButton.id === id) {
        this.buttons.splice(curButtonIndex, 1)
        return
      }
    }
  }

}

export interface ButtonbarEvent {
  event: string
}

export type ButtonbarStore = {
  stack: Array<ButtonbarStack>
  event: ButtonbarEvent | undefined
}

export const useButtonbarStore = defineStore('buttonbar', {
  state: () => {
    return {
      stack: [],
      event: undefined
    } as ButtonbarStore
  },
  getters: {},
  actions: {
    /**
     * Clear the whole stack
     */
    ClearStack() {
      this.stack = []
    },

    /**
     * Add of overwrite an existing button-stack
     * @param stackToAdd The current button-stack ready to use
     * @returns
     */
    AddStack(stackToAdd: ButtonbarStack) {
      // replace if already exist with the same ID
      for (const [currentButtonIndex, currentButton] of this.stack.entries()) {
        if (currentButton.ID() === stackToAdd.ID()) {
          this.stack.splice(currentButtonIndex, 1)
        }
      }

      this.stack.push(stackToAdd)
    },

    /**
     * Get the stack with id.
     * If id not exist, stack will be cleared and a new one will be created
     * @param id 
     * @returns 
     */
    Stack(id: string): ButtonbarStack {

      for (const [currentButtonIndex, currentButton] of this.stack.entries()) {
        if (currentButton.ID() === id) {
          return currentButton as ButtonbarStack
        }
      }

      this.ClearStack()
      let newStack = new ButtonbarStack(id, '')
      this.AddStack(newStack)
      return newStack
    },

    Current(): ButtonbarButton[] {
      if (this.stack.length === 0) {
        return [] as ButtonbarButton[]
      }
      return this.stack[this.stack.length - 1].Buttons()
    },

    /**
     * Get the current Button-Stack
     * @returns The current button stack or undefined
     */
    CurrentStack(): ButtonbarStack | undefined {
      if (this.stack.length > 0) {
        const currentStack = this.stack[this.stack.length - 1]
        return currentStack as ButtonbarStack
      }
      return
    },

    /**
     * Close the current button-stack and use the previose one if there is one
     */
    CloseCurrent() {
      this.stack.pop()
      if (this.stack.length > 0) {
        const currentStack = this.stack[this.stack.length - 1]
      }
    },

    Close(id: string) {
      for (const [currentButtonIndex, currentButton] of this.stack.entries()) {
        if (currentButton.ID() === id) {
          this.stack.splice(currentButtonIndex, 1)
        }
      }
    },

    TriggerEvent(event: string) {
      this.$patch({ event: { event: event } })
    }

  }
})
