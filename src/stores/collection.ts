import { defineStore } from 'pinia'
import { type JSONSingleDoc } from '@/api/document'
import { errors } from '@/api/error'


export type CollectionStore = {
  documents: Record<string, JSONSingleDoc>
}

export const useCollectionStore = defineStore('collection', {
  state: () => {
    const newStoreState: CollectionStore = {
      documents: {},
    }
    return newStoreState
  },

  persist: {
    key: 'dyndb_collection',
    paths: ['documents'],
  },

  getters: {},
  actions: {
    CollectDocument(doc: JSONSingleDoc | undefined) {
      if (doc?._links?.self === undefined) {
        throw errors.ParameterUndefined
      }

      this.documents[doc._links?.self] = doc
    },

    ForgetDocument(docID: string | undefined) {
      if (docID === undefined) return errors.ParameterUndefined
      delete this.documents[docID]
    }
  }
})
