import type { AttachmentAction } from "@/api/attachment";
import type { CollectorActionMode } from "@/api/collector";
import type { DocumentAction } from "@/api/document";
import { defineStore } from "pinia";

export type ConfigStore = {
  themeName: string;
  pageSize: number;
  documentAction: DocumentAction
  collectorActionMode: CollectorActionMode;
  attachmentAction: AttachmentAction;
  autoRefresh: Record<string, boolean>;
};


export const useConfigStore = defineStore("config", {
  state: () => {
    return {
      themeName: "default",
      pageSize: 25,
      documentAction: "view",
      collectorActionMode: 'select',
      attachmentAction: 'preview',
      autoRefresh: {},
    } as ConfigStore;
  },

  persist: {
    key: 'dyndb_configstore',
    //    paths: ['themeName', 'pageSize', ''],
  },

  actions: {
    SetTheme(theme: string) {
      this.$patch({
        themeName: theme,
      });
    },
    SetPageSize(newPageSize: number) {
      this.$patch({
        pageSize: newPageSize,
      });
    },
    SetDocumentAction(newAction: DocumentAction) {
      this.$patch({
        documentAction: newAction,
      });
    },
    SetActionMode(name: string, newAction: any) {
      let newObj = {} as any
      newObj[name] = newAction

      this.$patch(newObj);
    },
  },
});
