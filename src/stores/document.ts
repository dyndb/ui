import { defineStore } from 'pinia'
import { type JSONSingleDoc, type JSONSingleDocLinks } from '@/api/document'

export type DocumentStore = {
  document: JSONSingleDoc | undefined
  templateName: string | undefined
  isBuiltin: boolean | undefined
  links: JSONSingleDocLinks | undefined

  cached: Map<string, JSONSingleDoc>

  events: DocumentEvent | undefined
}

export type DocumentEvent = {
  event: 'loaded' | 'saved' | 'deleted' | 'error'
  value: JSONSingleDoc | Error | undefined
}

export const useDocumentStore = defineStore('document', {
  state: () => {
    return {
      cached: new Map<string, JSONSingleDoc>()
    } as DocumentStore
  },

  actions: {
    Loaded(doc: JSONSingleDoc) {
      this.$patch({
        events: {
          event: 'loaded',
          value: doc
        }
      })
    },
    Saved(doc: JSONSingleDoc) {
      this.$patch({
        events: {
          event: 'saved',
          value: doc
        }
      })
    },
    Deleted(doc: JSONSingleDoc) {
      this.$patch({
        events: {
          event: 'deleted',
          value: doc
        }
      })
    },

    /**
     * Set the state to error
     * @param err 
     */
    Error(err: Error) {
      this.$patch({
        events: {
          event: 'error',
          value: err
        }
      })
    }
  },

  getters: {
    SavePossible: (state: DocumentStore) => {
      return state.document?._links?.save !== undefined
    },
    DeletePossible: (state: DocumentStore) => {
      return state.document?._links?.delete !== undefined
    }
  }
})
