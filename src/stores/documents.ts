import { defineStore } from 'pinia'
import { type JSONSingleDoc } from '@/api/document'
import { type JSONPagedDocLinks } from '@/api/documents'

export type DocumentsStoreState = {
  //newDocument: JSONSingleDoc;
  documents: Array<JSONSingleDoc>
  filtered: boolean
  allDocuments: Record<string, JSONSingleDoc>
  links?: JSONPagedDocLinks
}

export type DocumentsStoreGetters = {
  Documents: (state: DocumentsStoreState) => JSONSingleDoc[]
  CreatePossible: (state: DocumentsStoreState) => boolean
  PrevPossible: (state: DocumentsStoreState) => boolean
  NextPossible: (state: DocumentsStoreState) => boolean
}

export type DocumentsStoreActions = {
  Append: (documents: JSONSingleDoc[]) => void
  Reset: (documents: JSONSingleDoc[]) => void
  Remove: (documents: JSONSingleDoc[]) => void
}

// export const DocumentsStores = new Map<string, any>()

export function CreateDocumentsStore(id: string) {
  const newStore = defineStore(id, {
    state: () => {
      const newStore = {
        documents: [],
        filtered: false,
        allDocuments: {},
        links: {}
      } as DocumentsStoreState

      return newStore
    },

    getters: {
      RefreshPossible: (state: DocumentsStoreState) => {
        return state.links?.self !== undefined
      },
      CreatePossible: (state: DocumentsStoreState) => {
        return state.links?.create !== undefined
      },
      PrevPossible: (state: DocumentsStoreState) => {
        return state.links?.prev !== undefined
      },
      NextPossible: (state: DocumentsStoreState) => {
        return state.links?.next !== undefined
      }
    },

    actions: {
      // Clean will remove all stored documents
      Clean() {
        this.allDocuments = {}
        this.documents = []
      },
      Append(docs: Record<string, JSONSingleDoc>) {
        const newDocuments = { ...this.allDocuments, ...docs }
        this.allDocuments = newDocuments
        this.documents = Array.from(Object.values(this.allDocuments))
      },
      Set(docs: Record<string, JSONSingleDoc>) {
        this.allDocuments = docs
        this.documents = Array.from(Object.values(this.allDocuments))
      },
      Remove(docKey: string) {
        delete this.allDocuments[docKey]
        this.documents = Array.from(Object.values(this.allDocuments))
      },
      Get(docKey: string) {
        return this.allDocuments[docKey]
      },
      FromSelfURL(selfURL: string | undefined): JSONSingleDoc | undefined {
        if (selfURL === undefined) {
          return undefined
        }

        for (const doc in this.documents) {
          if (this.documents[doc]._links?.self === selfURL) {
            return this.documents[doc]
          }
        }
      },
      Search(searchstring: string) {
        // reset the search
        if (searchstring === '') {
          if (this.filtered) {
            this.$patch({
              filtered: false,
              documents: Array.from(Object.values(this.allDocuments).values())
            })
          }
          return
        }

        // we need the single words
        const words = searchstring.split(' ')

        // get all docs
        let filteredDocuments = Array.from(Object.values(this.allDocuments).values())

        // filter docs
        for (const word of words) {
          filteredDocuments = filteredDocuments.filter((value: JSONSingleDoc) => {
            const templateJson = JSON.stringify(value).toLowerCase()
            const match = templateJson.includes(word.toLocaleLowerCase())
            return match
          })
        }

        // patch it
        this.$patch({
          filtered: true,
          documents: filteredDocuments
        })
      }
    }
  })

  // DocumentsStores.set(id, newStore)

  return newStore
}

export const useDocumentsStore = CreateDocumentsStore('documents')
