import { CreateDocumentsStore } from "./documents";

export const useFieldsStore = CreateDocumentsStore("fields");
