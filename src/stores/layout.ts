import { defineStore } from "pinia";

export type LayoutStore = {
  menuVisible: boolean;
  rightBladeComponent: BladeComponent | undefined;
};

export interface BladeComponent {
  name: string;
  props: any;
}

export const useLayoutStore = defineStore("layout", {
  state: () => {
    return {
      menuVisible: true,
    } as LayoutStore;
  },
  getters: {},
  actions: {},
});
