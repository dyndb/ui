import { defineStore } from 'pinia'
import { type DocLinksElement, type DocLinksElementsLinks } from '@/api/links'

export type DocLinksStore = {
  doclinks: Map<string, DocLinksElement>
  links?: DocLinksElementsLinks
}

export const useDocLinksStore = defineStore('doclinks', {
  state: () => {
    return {
      doclinks: new Map<string, DocLinksElement>(),
      links: undefined
    } as DocLinksStore
  },
  getters: {},
  actions: {}
})
