import { defineStore } from 'pinia'

export type LoginStore = {
  loggedin: boolean | undefined

  username: string
}
export const useLoginStore = defineStore('login', {
  state: () => {
    return {
      loggedin: undefined,

      username: ''
    } as LoginStore
  },
  actions: {}
})
