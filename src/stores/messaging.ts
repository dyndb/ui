import { defineStore } from 'pinia'

export interface MessagingData {
  topic?: string | undefined
  payload?: string | Object | undefined
}

export type MessagingStore = {
  connected: boolean
  message?: MessagingData
}

export const useMessagingStore = defineStore('messaging', {
  state: () => {
    return {
      connected: false,
      message: undefined
    } as MessagingStore
  },
  getters: {},
  actions: {
    Set(topic: string, payload: string | Object) {
      console.debug('Set', topic, payload)
      this.$state.message = {
        topic: topic,
        payload: payload
      } as MessagingData
    }
  }
})
