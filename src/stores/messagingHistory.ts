import { defineStore } from 'pinia'
import { type MessagingData } from './messaging'

export type MessagingHistoryStore = {
  messages: MessagingData[]
}

export const useMessagingHistoryStore = defineStore('messagingHistory', {
  state: () => {
    return {
      messages: [] as MessagingData[]
    } as MessagingHistoryStore
  },
  getters: {},
  actions: {
    Add(message: MessagingData | undefined) {
      if (message !== undefined) {
        console.debug('Add', JSON.stringify(message))

        const messages = this.messages

        messages.push(message)
        if (messages.length > 10) {
          messages.shift()
        }
        console.debug('Array', messages)
        this.$patch({
          messages: messages
        })
      }
    }
  }
})
