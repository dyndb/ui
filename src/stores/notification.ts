import { defineStore } from 'pinia'

export interface NotificationMessage {
  color?: string | undefined
  message?: string | undefined
  detail?: string | undefined
}

export type NotificationStore = {
  message?: NotificationMessage
}

export const useNotificationStore = defineStore('notificationStore', {
  state: () => {
    return {
      message: undefined
    } as NotificationStore
  },
  getters: {},
  actions: {
    Show(color: string, message: string, detail?: string) {
      this.message = {
        color: color,
        message: message,
        detail: detail
      } as NotificationMessage
    },
    ShowError(err: Error) {
      this.message = {
        color: 'red',
        message: err.message,
        detail: err.stack
      }
    }
  }
})
