import { CreateDocumentsStore } from './documents'

export const useTemplatesStore = CreateDocumentsStore('templates')
