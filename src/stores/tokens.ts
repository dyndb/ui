import { defineStore } from 'pinia'
import { type JwtPayload, jwtDecode } from 'jwt-decode'

export interface AccessTokenClaims {
  refresh_in_seconds: number | undefined
}

export type TokenStore = {
  RefreshToken: string | undefined
  TefreshTokenTimerSeconds: number
  RefreshTokenTimer: number | undefined
}

export const useTokenStore = defineStore('refreshtoken', {
  state: () => {
    const newStorage = {
      RefreshToken: undefined,
      TefreshTokenTimerSeconds: 30,
      RefreshTokenTimer: undefined
    } as TokenStore

    return newStorage
  },

  persist: {
    key: 'dyndb_tokens',
    paths: ['RefreshToken', 'TefreshTokenTimerSeconds'],
  },

  actions: {


    /**
    Decode the jwtToken, read refresh_in_seconds
    */
    async RefreshTokenSet(jwtToken: string) {
      // parse the jwt and read the timeout
      const decoded = jwtDecode<AccessTokenClaims>(jwtToken)
      if (decoded.refresh_in_seconds !== undefined) {
        this.TefreshTokenTimerSeconds = decoded.refresh_in_seconds
      } else {
        throw new Error('refresh_in_seconds missing in jwt')
      }

      // save the token
      this.RefreshToken = jwtToken
    },

    // wait until refresh token needs to be refreshed and call callback
    RefreshTokenOnTimeout(callback: (currentToken: string) => Promise<void>) {
      // cancel
      if (this.RefreshTokenTimer !== undefined) {
        clearTimeout(this.RefreshTokenTimer)
        this.RefreshTokenTimer = undefined

        console.debug(`token: cancel running timer`)
      }

      // timer
      console.debug(`Refresh token in ${this.TefreshTokenTimerSeconds} seconds`)
      this.RefreshTokenTimer = setTimeout(async () => {
        console.debug(`token: timeout`)

        // call
        if (this.RefreshToken !== undefined) {
          await callback(this.RefreshToken)
        }

        this.RefreshTokenTimer = undefined
      }, this.TefreshTokenTimerSeconds * 1000)
    }
  }
})
