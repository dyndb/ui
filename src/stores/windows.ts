import { defineStore } from 'pinia'
import { type DynWindow } from '../api/windows'
import { v4 } from 'uuid'

export type WindowsStore = {
  windows: Record<string, DynWindow>
  windowIDs: Set<string>
  windowActive: DynWindow | undefined
  WindowStack: Array<DynWindow>
}

export const useWindowStore = defineStore('windows', {
  state: () => {
    return {
      windows: {},
      windowIDs: new Set<string>(),
      windowActive: undefined,
      WindowStack: []
    } as WindowsStore
  },

  persist: {
    key: 'dyndb_windows',
    paths: ['windows.dynamic'],
  },

  getters: {},
  actions: {

    Save(parentID: string, win: DynWindow) {
      let parentWindow: DynWindow | undefined

      // try to add to an parent
      if (parentID !== '') {
        // find parent
        parentWindow = this.FindWindow(parentID)

        // not exist, create a new parent
        if (parentWindow === undefined) {
          parentWindow = {
            ID: parentID,
            displayName: parentID,
            childs: {}
          } as DynWindow

          // add new parent
          this.windows[parentID] = parentWindow
        }
      } else {
        parentWindow = {} as DynWindow
        parentWindow.childs = this.windows
      }

      // calculate index
      win.index = Object.keys(parentWindow.childs as Object).length

      if (parentWindow.childs !== undefined) {
        parentWindow.childs[win.ID] = win
      }

    },

    FindRecursive(id: string, windows: Record<string, DynWindow>): DynWindow | undefined {
      if (id in windows) {
        return windows[id]
      }

      let windowFound: DynWindow | undefined = undefined

      for (const windowID in windows) {
        const window = windows[windowID]
        if (windowFound !== undefined) {
          return
        }

        if (window.childs !== undefined) {
          windowFound = this.FindRecursive(id, window.childs)
          if (windowFound !== undefined) {
            return windowFound
          }
        }
      }

      return windowFound
    },

    FindWindow(id: string): DynWindow | undefined {
      return this.FindRecursive(id, this.windows)
    },

    SetActive(id: string) {
      var curWindow = this.FindWindow(id)

      if (curWindow !== undefined) {

        // clone this window
        if (curWindow.createInstance) {
          // clone
          let newWindow = Object.assign({}, curWindow)
          // update distinct values
          newWindow.ID = v4()
          newWindow.closable = true
          newWindow.createInstance = false
          newWindow.state = Object.assign({}, curWindow.state)
          this.Save('dynamic', newWindow)
          curWindow = newWindow
        }

        // Init
        if (curWindow.prepare !== undefined) {
          curWindow.prepare(curWindow)
        }

        this.WindowStack.push(curWindow)
        this.windowActive = curWindow
      }
    },

    RemoveWindowRecursive(id: string, windows: Record<string, DynWindow>) {
      if (id in windows) {
        delete windows[id]
        return
      }

      for (const windowID in windows) {
        const window = windows[windowID]
        if (window.childs !== undefined) {
          this.RemoveWindowRecursive(id, window.childs)
        }
      }


    },

    RemoveWindow(id: string | undefined) {
      if (id === undefined) return
      // remove from tree
      this.RemoveWindowRecursive(id, this.windows)

      // remove current window
      if (this.windowActive?.ID === id) {
        this.windowActive = undefined
      }

      // remove from stack
      this.WindowStack.forEach((window: DynWindow, index: number) => {
        if (window.ID === id) {
          this.WindowStack.splice(index, 1)
        }
      })

      // set the last window in stack as active
      if (this.WindowStack.length > 0) {
        this.windowActive = this.WindowStack[this.WindowStack.length - 1]
      }

    }
  }
})
