import { fileURLToPath, URL } from 'node:url'

import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

// env-vars
const mode = process.env.DEV_MODE

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: { transformAssetUrls }
    }),

    // @quasar/plugin-vite options list:
    // https://github.com/quasarframework/quasar/blob/dev/vite-plugin/index.d.ts
    quasar()
  ],

  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@/api': fileURLToPath(new URL('./src/api', import.meta.url)),
      '@/stores': fileURLToPath(new URL('./src/stores', import.meta.url))
    }
  },

  base: '/ui/',

  server: {
    port: 3000,
    proxy:
      mode === 'proxy'
        ? {
          '/api': {
            target: 'http://127.0.0.1:3535',
            changeOrigin: true,
            secure: false
            //rewrite: (path) => path.replace(/^\/api/, '')
          }
        }
        : undefined
  },

  build: {
    rollupOptions: {
      output: mode !== 'proxy' ? {
        manualChunks: function manualChunks(id) {
          if (id.includes('node_modules')) {
            return 'vendor'
            // Find out the biggest modules:
            let nodeModulesIndex = id.indexOf('node_modules')
            let SubModuleEndIndex = id.indexOf("/", nodeModulesIndex + 13)
            let SubModuleName = id.slice(nodeModulesIndex, SubModuleEndIndex)
            SubModuleName = SubModuleName.replaceAll('/', '_')
            SubModuleName = SubModuleName.replaceAll('@', '')
            return "vendor_" + SubModuleName;
          }
        }
      } : undefined,
    }
  }
})
